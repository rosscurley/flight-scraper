from python_packages.scraper_middleware.exceptions import FlightdataError, MiddlewareError, LoggingError
from python_packages.scraper_middleware.flightdata_middleware import Flightdata_Middleware
from python_packages.scraper_middleware.logging_middleware import Logging_Middleware

class Test_Data:
    def __init__(self):
        # create middleware object
        self.mw = Flightdata_Middleware('database.ini', 'postgresql')
        self.tests = 0
        self.fails = 0
        self.success = 0

    def test(self):
        try:
            self.test_insert_airports()
        except Exception as e:
            print(e)
        try:
            self.test_insert_aircrafts()
        except Exception as e:
            print(e)
        try:
            self.test_insert_airlines()
        except Exception as e:
            print(e)
        try:
            self.test_insert_flightinfo()
        except Exception as e:
            print(e)
        try:
            self.test_insert_flightstatus()
        except Exception as e:
            print(e)
        try:
            self.test_insert_airfare()
        except Exception as e:
            print(e)
        try:
            self.test_insert_trackedroutes()
        except Exception as e:
            print(e)

        print("""
        {0} tests conducted
        {1} succeeded
        {2} failed""".format(self.tests, self.success, self.fails))

        self.mw.close_connection()

    def test_insert_airports(self):
        self.tests = self.tests + 1
        try:
            # insert airports
            self.mw.insert_airport("CNS", "-16.8858883", "145.755278", "Cairns Airport", "BRI")
            airports = (
                ("PER", "-31.9385", "115.9672", "Perth Airport", "PER"),
                ("SYD", "-33.9399", "151.1753", "Sydney Airport", "SYD"),
                ("KNX", "-15.778056", "128.7075", "Kununurra Airport", "PER"),
                ("LEA", "22.235833", "114.088611", "Learmonth Airport", "PER")
            )
            self.mw.insert_airport_list(airports)
        except MiddlewareError as err:
            print("test_insert_airports failed : ", err.errors)
            self.fails = self.fails +1
            return

        self.success = self.success +1

    def test_insert_aircrafts(self):
        self.tests = self.tests + 1
        try:
            # insert aircrafts
            self.mw.insert_aircraft("Airbus", "A330", "335")
            aircrafts = (
                ("Airbus","A320","220"),
                ("Airbus","A380","555"),
                ("Boeing","737","137")
            )
            self.mw.insert_aircraft_list(aircrafts)
        except MiddlewareError as err:
            print("test_insert_aircrafts failed : ", err.errors)
            self.fails = self.fails +1
            return

        self.success = self.success +1

    def test_insert_airlines(self):
        self.tests = self.tests + 1
        try:
            # insert airlines
            self.mw.insert_airline("Qantas", "www.qantas.com.au")
            airlines = (
                ("AirNorth", "www.airnorth.com.au"),
                ("Jetstar", "www.jetstar.com.au"),
                ("REX", "www.rex.com.au"),
                ("Tigerair", "www.tigerair.com.au"),
                ("Virgin", "www.virgin.com.au")
            )
            self.mw.insert_airline_list(airlines)
        except MiddlewareError as err:
            print("test_insert_airlines failed : ", err.errors)
            self.fails = self.fails +1
            return

        self.success = self.success +1

    def test_insert_flightinfo(self):
        self.tests = self.tests + 1
        try:
            # insert flightinfo
            self.mw.insert_flightinfo("QF574","2019-08-20 05:15","2019-08-20 11:20","PER","SYD","Qantas","Airbus", "A330")
            flightinfo = (
                ("QF576","2019-08-20 07:20","2019-08-20 13:25","PER","SYD","Qantas","Boeing" ,"737"),
                ("QF642","2019-08-20 10:25","2019-08-20 16:30","PER","SYD","Qantas","Airbus" ,"A330"),
                ("QF580","2019-08-20 12:45","2019-08-20 18:50","PER","SYD","Qantas","Airbus" ,"A330")
            )
            self.mw.insert_flightinfo_list(flightinfo)
        except MiddlewareError as err:
            print("test_insert_flightinfo failed : ", err.errors)
            self.fails = self.fails +1
            return

        self.success = self.success +1
    def test_insert_airfare(self):
        self.tests = self.tests + 1
        try:
            # insert airfare
            self.mw.insert_airfare("QF576", "2019-08-20 07:43", "Red e-Deal","One Way", "593")
            airfares = (
                ("QF576", "2019-08-20 15:20", "Flex", "One Way", "770"),
                ("QF576", "2019-08-20 13:12", "Business", "One Way", "2520"),
                ("QF642", "2019-08-20 23:09", "Red e-Deal", "One Way", "506"),
                ("QF642", "2019-08-20 17:30", "Flex", "One Way", "770")
            )
            self.mw.insert_airfare_list(airfares)
        except MiddlewareError as err:
            print("test_insertairfare failed : ", err.errors)
            self.fails = self.fails +1
            return

        self.success = self.success +1

    def test_insert_trackedroutes(self):
        self.tests = self.tests + 1
        try:
            # insert trackedroute
            self.mw.insert_trackedroute("PER", "SYD", "2019-08-10 15:20", "2019-08-26 15:20")
            trackedroutes = (
                ("PER", "KNX", "2019-08-10 15:20", "2019-08-26 15:20"),
                ("SYD", "KNX", "2019-08-10 15:20", "2019-08-26 15:20")
            )
            self.mw.insert_trackedroute_list(trackedroutes)
        except MiddlewareError as err:
            print("test_insert_trackedroutes failed :", err.errors)
            self.fails = self.fails +1
            return

        self.success = self.success +1
        
    def test_insert_flightstatus(self):
        self.tests = self.tests + 1
        try:
            # insert flightstatus
            self.mw.insert_flightstatus("QF576", "2019-08-20 11:00", "2019-08-20 10:25", "scheduled", "2019-08-20 16:30", "scheduled")
            flightstatus = (
                ("QF642", "2019-08-20 08:00", "2019-08-20 07:20", "scheduled", "2019-08-20 13:25 ", "scheduled"),
                ("QF580", "2019-08-20 14:00", "2019-08-20 12:45", "scheduled", "2019-08-20 18:50 ", "scheduled")
            )
            self.mw.insert_flightstatus_list(flightstatus)
        except MiddlewareError as err:
            print("test_insert_flightstatus failed :", err.errors)
            self.fails = self.fails +1
            return

        self.success = self.success +1
    
class Test_Logging:
    def __init__(self):
        # create middleware object
        self.mw = Logging_Middleware('database.ini', 'postgresql')
        self.tests = 0
        self.fails = 0
        self.success = 0

    def test(self):
        try:
            self.test_insert_setting()
        except Exception as e:
            print(e)
        try:
            self.test_insert_session()
        except Exception as e:
            print(e)

        try:
            self.test_insert_log()
        except Exception as e:
            print(e)

        try:
            self.test_insert_error()
        except Exception as e:
            print(e)

        print("""
        {0} tests conducted
        {1} succeeded
        {2} failed""".format(self.tests, self.success, self.fails))
        
        self.mw.close_connection()
    
    def test_insert_setting(self):
        self.tests = self.tests + 1
        try:
            # insert setting
            self.mw.insert_setting("5b9b947d-58b7-4249-b1cb-cbccf2645589", "2019-08-20 11:00", "Test", "2019-09-02 10:25")
            settings = (
                ("64bd0a21-8240-409d-8d74-19538347e007", "2019-09-20 08:00", "TEST", "2019-10-20 07:20"),
                ("e85b084d-8a8b-42bf-b55c-f7fd83cc46b1", "2019-10-20 14:00", "TEST", "2019-11-20 12:45")
            )
            self.mw.insert_setting_list(settings)
        except MiddlewareError as err:
            print("test_insert_setting failed :", err.errors)
            self.fails = self.fails +1
            return
 
        self.success = self.success +1

    def test_insert_session(self):
        self.tests = self.tests + 1
        try:
            # insert session
            self.mw.insert_session("www.qantas.com", "2019-08-20 11:00", "2019-08-20 10:25", "n", "249.54.229.29")
            sessions = (
                ("www.jetstar.com", "2019-08-20 11:00", "2019-08-20 10:25", "f", "130.43.1.110"),
                ("www.virginaustralia.com", "2019-08-20 14:00", "2019-08-20 12:45", "r", "72.229.3.141")
            )
            self.mw.insert_session_list(sessions)
        except MiddlewareError as err:
            print("test_insert_session failed :", err.errors)
            self.fails = self.fails +1
            return
        
        self.success = self.success +1

    def test_insert_log(self):
        self.tests = self.tests + 1
        try:
            # insert log
            self.mw.insert_log("2019-08-20 11:30", "www.qantas.com", "www.qantas.com", "2019-08-20 11:00", "QF576", "F", "", "S")
            sessions = (
                ("2019-08-20 10:25", "www.jetstar.com", "www.jetstar.com", "2019-08-20 11:00", "QF642", "A", "", "S"),
                ("2019-08-20 12:45", "www.virginaustralia.com", "www.virginaustralia.com", "2019-08-20 14:00", "QF580", "A", "", "F")
            )
            self.mw.insert_log_list(sessions)
        except MiddlewareError as err:
            print("test_insert_session failed :", err.errors)
            self.fails = self.fails +1
            return

        self.success = self.success +1

    def test_insert_error(self):
        self.tests = self.tests + 1
        try:
            # insert log
            self.mw.insert_error("d1fbad72-77e7-4333-b2c8-aa6a93519812", "2019-08-20 11:30", "www.qantas.com", "F", "TEST")
            sessions = (
                ("a694d6be-302b-4e59-b723-260c172f0ad6", "2019-08-20 10:25", "www.jetstar.com", "E", "TEST"),
                ("31960713-8620-45c2-bed8-17d736f8579c", "2019-08-20 12:45", "www.virginaustralia.com", "R", "TEST")
            )
            self.mw.insert_error_list(sessions)
        except MiddlewareError as err:
            print("test_insert_session failed :", err.errors)
            self.fails = self.fails +1
            return

        self.success = self.success +1

if __name__ == '__main__':
    test1 = Test_Data()
    test1.test()
    test2 = Test_Logging()
    test2.test()
    
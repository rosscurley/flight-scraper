# jetstar.py - Spider for Jetstar flight data from Google flights
# Author - Dean Quaife
# Last modified - 25/10/19
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# To run this spider:
# scrapy crawl jetstar -a date=YYYY-MM-DD -a days=DAYS -a origin=ORIGIN -a dest=DEST
# to output scraped flight data to a json format add -o flights.json
# only accounts for one way flights

import scrapy
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, ElementClickInterceptedException
import time
import datetime
from scraper_middleware.flightdata_middleware import Flightdata_Middleware
from general.timezones import TimezoneConverter

class JetstarSeleniumSpider(scrapy.Spider):
    name = 'jetstar'
    user_agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36(KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36'
    allowed_domains = ['google.com/flights']

    def __init__(self, date, days, origin, dest, *args, **kwargs):
        self.driver = webdriver.Chrome(executable_path=r'C:\chromedriver.exe')
        self.date = date
        self.days = days
        self.origin = origin
        self.dest = dest

    def start_requests(self):
        url = 'https://www.google.com/flights?lite=0#flt=PER..2019-10-12*.PER.2019-10-16;c:AUD;e:1;a:3K,BL,GK,JQ*3K,BL,GK,JQ;sd:1;t:h'
        yield scrapy.Request(url, self.parse)

    def parse(self, response):
        #load webpage and wait a short while for javascript to load
        self.driver.get(response.url)
        time.sleep(2.5)
        #select origin
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[1]/div[4]/div/div[3]/div/div[2]/div[1]')
        next.click()
        time.sleep(1)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/destination-picker/div[1]/div[2]/div[2]/input').send_keys(self.origin)
        time.sleep(1)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/destination-picker/div[4]/div/div[2]/div[1]/div/ul/li[1]/div/div[1]/div[1]')
        next.click()
        #select destination
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[1]/div[4]/div/div[3]/div/div[2]/div[2]')
        next.click()
        time.sleep(1)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/destination-picker/div[1]/div[2]/div[2]/input').send_keys(self.dest)
        time.sleep(1)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/destination-picker/div[4]/div/div[2]/div[1]/div/ul/li[1]/div/div[1]/div[1]/span[1]')
        next.click()
        #select one-way
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[1]/div[4]/div/div[3]/div/div[1]/div[1]/dropdown-menu/div/div[1]')
        check = False
        #some try blocks are required to handle clicking on previous elements accidentally before they unload
        while not check:
            try:
                next.click()
                check = True
            except ElementClickInterceptedException:
                pass
        time.sleep(1)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[1]/div[4]/div/div[3]/div/div[1]/div[1]/dropdown-menu/div/div[2]/menu-item[2]/span')
        next.click()
        #specify date and search
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[1]/div[4]/div/div[3]/div/div[2]/div[4]/div[1]/div[2]')
        next.click()
        time.sleep(1)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/div[4]/div[2]/div[1]/date-input/input').send_keys(self.date)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/div[5]/g-raised-button')
        next.click()
        #filter by Jetstar flights only
        time.sleep(2)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[4]/div[2]/div/div[1]/div[4]/div/div/filter-chip[3]/span[1]')
        next.click()
        time.sleep(1)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/div[2]/div/div[1]/g-selection-control-switch')
        next.click()
        #specify Jetstar using xpath to find the matching attribute (JQ)
        next = self.driver.find_element_by_xpath("//li[@data-aid='JQ']")
        next.click()
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/div[1]/div[2]')
        next.click()
        #non-stop filter
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[2]/main[4]/div[2]/div/div[1]/div[4]/div/div/filter-chip[2]')
        check = False
        while not check:
            try:
                next.click()
                check = True
            except ElementClickInterceptedException:
                pass
        time.sleep(1)
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/div[2]/div/ol/li[2]')
        next.click()
        next = self.driver.find_element_by_xpath('/html/body/div[2]/div[2]/div/div[2]/div[3]/div/jsl/div/div[5]/div/div[1]/div[2]')
        next.click()
        time.sleep(2)
        middleware = Flightdata_Middleware()
        currTime = TimezoneConverter().time_to_utc('perth', datetime.datetime.now())
        for page in range(int(self.days)):            
            #iterate through each entry on the page
            #Note: sometimes a user satisfaciton survey appears on page, blocking the results   
            i = 1 #tracks element on page
            for flight in self.driver.find_elements_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li'):
                next = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('+ str(i) + ') > div > div.gws-flights-widgets-expandablecard__header.gws-flights-results__itinerary-card-header > div.gws-flights-results__itinerary-card-summary.gws-flights-results__result-item-summary.gws-flights__flex-box > div.gws-flights-results__expand')
                check = False
                while not check:
                    try:
                        next.click()
                        check = True
                    except ElementClickInterceptedException:
                        pass
                time.sleep(1.5)
                self.departureDate = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('+ str(i) + ') > div > div.gws-flights-widgets-expandablecard__header.gws-flights-results__itinerary-card-header > div.gws-flights-results__itinerary-card-summary.gws-flights-results__result-item-summary.gws-flights__flex-box > div.gws-flights-results__select-header.gws-flights__flex-filler > div.gws-flights-results__expanded-itinerary.gws-flights-results__itinerary > div.gws-flights-results__itinerary-details-heading.gws-flights__flex-box.gws-flights__flex-filler.gws-flights__align-center.flt-subhead1 > div > span:nth-child(3)').text
                self.airfare = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li.gws-flights-results__result-item.gws-flights__flex-box.gws-flights-results__result-item-more.gws-flights-results__expanded > div > div.gws-flights-widgets-expandablecard__header.gws-flights-results__itinerary-card-header > div.gws-flights-results__itinerary-card-summary.gws-flights-results__result-item-summary.gws-flights__flex-box > div.gws-flights-results__select-header.gws-flights__flex-filler > div.gws-flights-results__expanded-itinerary.gws-flights-results__itinerary > div.gws-flights-results__itinerary-price > div').text
                self.depart_iata = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('+ str(i) + ') > div > div.gws-flights-widgets-expandablecard__body > div.gws-flights-widgets-expandablecard__content.gws-flights-results__expanded-details-content > div:nth-child(1) > div > div.gws-flights-results__leg-details.gws-flights__flex-filler > div.gws-flights-results__leg-itinerary > div.gws-flights-results__leg-departure.gws-flights__flex-box.flt-subhead1Normal > div:nth-child(3) > span.gws-flights-results__iata-code').text
                self.arrival_iata = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('+ str(i) + ') > div > div.gws-flights-widgets-expandablecard__body > div.gws-flights-widgets-expandablecard__content.gws-flights-results__expanded-details-content > div:nth-child(1) > div > div.gws-flights-results__leg-details.gws-flights__flex-filler > div.gws-flights-results__leg-itinerary > div.gws-flights-results__leg-arrival.gws-flights__flex-box.flt-subhead1Normal > div:nth-child(3) > span.gws-flights-results__iata-code').text
                self.departureTime = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('+ str(i) + ') > div > div.gws-flights-widgets-expandablecard__body > div.gws-flights-widgets-expandablecard__content.gws-flights-results__expanded-details-content > div:nth-child(1) > div > div.gws-flights-results__leg-details.gws-flights__flex-filler > div.gws-flights-results__leg-itinerary > div.gws-flights-results__leg-departure.gws-flights__flex-box.flt-subhead1Normal > div:nth-child(1) > span > span').text
                self.arrivalTime = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('+ str(i) + ') > div > div.gws-flights-widgets-expandablecard__body > div.gws-flights-widgets-expandablecard__content.gws-flights-results__expanded-details-content > div:nth-child(1) > div > div.gws-flights-results__leg-details.gws-flights__flex-filler > div.gws-flights-results__leg-itinerary > div.gws-flights-results__leg-arrival.gws-flights__flex-box.flt-subhead1Normal > div:nth-child(1) > span > span').text
                #some information may be missing; on exception, leave the field empty
                try:
                    self.airline = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('+ str(i) + ') > div > div.gws-flights-widgets-expandablecard__body > div.gws-flights-widgets-expandablecard__content.gws-flights-results__expanded-details-content > div:nth-child(1) > div > div.gws-flights-results__leg-details.gws-flights__flex-filler > div.gws-flights-results__leg-flight.gws-flights__flex-box.gws-flights__align-center.flt-caption > div:nth-child(1)').text
                except NoSuchElementException:
                    self.airline = ""
                try:
                    self.flightNo = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('+ str(i) + ') > div > div.gws-flights-widgets-expandablecard__body > div.gws-flights-widgets-expandablecard__content.gws-flights-results__expanded-details-content > div:nth-child(1) > div > div.gws-flights-results__leg-details.gws-flights__flex-filler > div.gws-flights-results__leg-flight.gws-flights__flex-box.gws-flights__align-center.flt-caption > div.gws-flights-results__other-leg-info.gws-flights__flex-box.gws-flights__align-center > span').text
                except NoSuchElementException:
                    self.flightNo = ""
                try:
                    self.aircraftModel = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__flex-grow.gws-flights-results__results.gws-flights__flex-column.gws-flights__scrollbar-padding > div.gws-flights-results__results-container.gws-flights__center-content > div.gws-flights__flex-grow.gws-flights-results__slice-results-desktop > div:nth-child(3) > div:nth-child(1) > ol > li:nth-child('+ str(i) + ') > div > div.gws-flights-widgets-expandablecard__body > div.gws-flights-widgets-expandablecard__content.gws-flights-results__expanded-details-content > div:nth-child(1) > div > div.gws-flights-results__leg-details.gws-flights__flex-filler > div.gws-flights-results__leg-flight.gws-flights__flex-box.gws-flights__align-center.flt-caption > div.gws-flights-results__other-leg-info.gws-flights__flex-box.gws-flights__align-center > div.gws-flights-results__aircraft-type > span:nth-child(1)').text
                except NoSuchElementException:
                    self.aircraftModel = ""
                middleware.insert_aircraft(self.aircraftModel.split(' ')[0], ' '.join(self.aircraftModel.split(' ')[1:]), 0)
                middleware.insert_flightinfo(flight_number = self.flightNo,
                                             departure_time=self.departureTime,
                                             arrival_time = self.arrivalTime,
                                             origin = self.depart_iata,
                                             destination = self.depart_iata,
                                             airline = 'Jetstar',
                                             make = self.aircraftModel.split(' ')[0],
                                             model = ' '.join(self.aircraftModel.split(' ')[1:]))
                price = self.airfare.replace('$', '')
                middleware.insert_airfare(self.flightNo, currTime, self.airfare, 'One Way', int(price))
                i = i + 1
                next.click()
            #go to next results page
            next = self.driver.find_element_by_css_selector('#flt-app > div.gws-flights__flex-column.gws-flights__flex-grow > main.gws-flights__flex-column.gws-flights__active-tab.gws-flights__flights-search > div.gws-flights__form.gws-flights__scrollbar-padding > div > div.gws-flights-form__form-card > div.gws-flights__flex-box.gws-flights__align-center > div.gws-flights-form__input-container.gws-flights__flex-box.gws-flights__flex-filler.gws-flights-form__calendar-input.flt-body2 > div.flt-input.gws-flights__flex-box.gws-flights__flex-filler.gws-flights-form__departure-input > div.gws-flights-form__flipper > span.gws-flights-form__next')
            #scroll up to bring the date search bar back into view
            self.driver.execute_script("arguments[0].scrollIntoView(false);", next)
            time.sleep(1)
            next.click()
            time.sleep(2)
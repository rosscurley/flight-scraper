# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.
ROUTES = [
    ['brisbane', 'melbourne', 'sydney'], #Adelaide, index:0
    ['adelaide', 'cairns', 'melbourne', 'sydney'], #Brisbane
    ['brisbane', 'melbourne', 'sydney'], #Cairns
    ['adelaide', 'brisbane', 'cairns', 'perth', 'sydney'], #Melbourne
    ['melbourne', 'sydney'], #Perth
    ['adelaide', 'brisbane', 'cairns', 'melbourne', 'perth'], #Sydney
]
AIRPORTS = [
    'adelaide',
    'brisbane',
    'cairns',
    'melbourne',
    'perth',
    'sydney',
]
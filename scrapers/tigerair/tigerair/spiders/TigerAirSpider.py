'''
TigerAirSpider.py
Author - Ivandy Darmawan
Last modified - 28/10/2019

Scraper for TigerAir flight data
This scraper only scrape one way flight
To run spider:
    'scrapy crawl tigerair -a dep=<DEPARTURE> -a arr=<DESTINATION>'
remove dep and arr to scrape all available flights
to choose a date  add:
    '-a travel_date=<DD>,<MM>'
to output to a file
    '-o <filename>.json
'''
import base64
import calendar
import scrapy
from scrapy_splash import SplashRequest
from datetime import date, timedelta, datetime
from time import strptime
import re
import os.path
#from tigerair.items import Airfareloader, FlightInfoLoader, FlightStatusLoader

#==from tigerair/__init__.py
from tigerair import AIRPORTS
from tigerair import ROUTES

#==python_packages
from general.user_agents import user_agents
from general.airport_data import AirportData
from general.timezones import TimezoneConverter
from general.logging_spider import LoggingSpider
from scraper_middleware.flightdata_middleware import Flightdata_Middleware

class InvalidArgError(Exception):
    pass

def departure(i):
    switcher={
        'adelaide':0,
        'brisbane':1,
        'cairns':2,
        'melbourne':3,
        'perth':4,
        'sydney':5
        }
    return switcher.get(i, 'Invalid Origin')

class TigerAirSpider(LoggingSpider):
    name = 'tigerair'
    link = r'https://booking.tigerair.com.au/TigerAirIBE/Booking/search'

    #taking the lua script from a file
    fn = os.path.join(os.path.dirname(__file__), "script.lua")
    with open(fn) as file:
        script = file.read()
        
    #program init to take in arguments
    def __init__(self, dep=None, arr=None, travel_date=None, user_agent='random', debug=False, *args, **kwargs):
        super(TigerAirSpider, self).__init__(*args, **kwargs)
        self.argsErr = False
        self.multiple_flights = False
        self.sixtyDays = False
        self.debug = debug
        self.user_agent = user_agents[user_agent]
        try:
            #if departure or arrival is not specified scrape all flights
            if dep is None and arr is None:
                #initialize the variables to True and lists
                self.multiple_flights = True
                self.flights = []
                self.dep = []
                self.arr = []

                #set departures from all aiports
                for i in AIRPORTS:
                    self.dep.append(i)
                
                #set arrival in the flights
                for i in range(len(self.dep)):
                    for ii in ROUTES[i]:
                        self.arr.append(ii)
                    self.flights.append(self.arr)
                    self.arr = [] 

            #if departure airport is invalid
            elif departure(dep.lower()) == 'Invalid Origin':
                print("Invalid Origin/Departure")
                raise InvalidArgError()

            #scrape all dest from given origin
            elif arr is None:
                self.multiple_flights = True
                self.flights = []
                self.dep = [dep.lower()]
                self.arr = []
                dep_index = departure(dep.lower())
                for i in ROUTES[dep_index]:
                    self.arr.append(i)
                self.flights.append(self.arr)
                self.arr = []

            #if dep and arr are specified and everything is good to go
            else:
                dep_index = departure(dep.lower())
                if arr.lower() not in ROUTES[dep_index]:
                    print(f"{dep.lower()}-{arr.lower()} route not available")
                    raise InvalidArgError()
                else:
                    self.dep = dep.lower()
                    self.arr = arr.lower()
                    print(f"{dep.lower()}-{arr.lower()} route available")


            #if flight date is not specified, scrape on the 60th day from today
            if travel_date is None:
                self.sixtyDays = True
            else:
                self.flight_day = int(travel_date.split(',')[0])
                self.flight_month = calendar.month_name[int(travel_date.split(',')[1])]

        except InvalidArgError:
            self.argsErr = True

    def start_requests(self):
        if self.argsErr:
            self.reportError(datetime.utcnow(), self.link, "fatal", "Routes is not available")
            scrapy.exceptions.CloseSpider("Argument Error")

        elif(self.multiple_flights and self.sixtyDays):
            count = 0
            for each_dep in self.flights:
                for each_arr in each_dep:
                    for i in range(60):
                        print('============= ' + f'scraping {self.dep[count]}-{each_arr} for {self.flight_month} {self.flight_day}...')
                        splash_args = {
                            'wait' : 2,
                            'html' : 1,
                            'images' : 1,
                            'lua_source': self.script,
                            'dep': self.dep[count],
                            'arr': each_arr,
                            'day': self.flight_day,
                            'month': self.flight_month,
                            'timeout' : 90
                        }

                        yield SplashRequest(
                                self.link,
                                endpoint = 'execute',
                                args = splash_args,
                                cache_args = ['lua_source'],
                                callback = self.parse_result_page,
                                session_id=1
                        )
                        self.flight_day = (date.today() + timedelta(days=i)).day
                        self.flight_month = calendar.month_name[(date.today() + timedelta(days=i)).month]
                count += 1

        elif(self.multiple_flights):
            count = 0
            for each_dep in self.flights:
                for each_arr in each_dep:
                    print('============= ' + f'scraping {self.dep[count]}-{each_arr} for {self.flight_month} {self.flight_day}...')
                    splash_args = {
                        'wait' : 2,
                        'html' : 1,
                        'images' : 1,
                        'lua_source': self.script,
                        'dep': self.dep[count],
                        'arr': each_arr,
                        'day': self.flight_day,
                        'month': self.flight_month,
                        'timeout' : 90
                    }
                    yield SplashRequest(
                        self.link,
                        endpoint = 'execute',
                        args = splash_args,
                        cache_args = ['lua_source'],
                        callback = self.parse_result_page,
                        session_id=1
                    )
                count += 1
        elif(self.sixtyDays):
            for i in range(60):
                self.flight_day = (date.today() + timedelta(days=i)).day
                self.flight_month = calendar.month_name[(date.today() + timedelta(days=i)).month]
                print('============= ' + f'scraping {self.dep}-{self.arr} on {self.flight_month} {self.flight_day}...')
                splash_args = {
                    'wait' : 2,
                    'html' : 1,
                    'images' : 1,
                    'lua_source': self.script,
                    'dep': self.dep,
                    'arr': self.arr,
                    'day': self.flight_day,
                    'month': self.flight_month,
                    'timeout' : 90
                }

                yield SplashRequest(
                    self.link,
                    endpoint = 'execute',
                    args = splash_args,
                    cache_args = ['lua_source'],
                    callback = self.parse_result_page,
                    session_id=1
                )
        else:
            print('============= ' + f'scraping {self.dep}-{self.arr} on {self.flight_month} {self.flight_day}...')
            splash_args = {
                'wait' : 2,
                'html' : 1,
                'images' : 1,
                'lua_source': self.script,
                'dep': self.dep,
                'arr': self.arr,
                'day': self.flight_day,
                'month': self.flight_month,
                'timeout' : 90
            }

            yield SplashRequest(
                self.link,
                endpoint = 'execute',
                args = splash_args,
                cache_args = ['lua_source'],
                callback = self.parse_result_page,
                session_id=1
            )

    def parse_result_page(self, response):
        timestamp = datetime.utcnow()
        endUrl = response.data["url"]
        status = 'success'
        if(response.xpath('//td[@class="no-flights"]')):
            print("No more flight")
            self.logScraping(timestamp, endUrl, "unknown", "airfare", status, f"No more flight on {self.flight_month} {self.flight_day}")
            self.onSessionEnd()
        else:
            origin = response.xpath(r"//*[@id='departSection']/div[1]/span[2]/text()").get()
            destination = response.xpath(r"//*[@id='departSection']/div[3]/span[2]/text()").get()
            dep_airport_data = AirportData(origin.lower())
            dest_airport_data = AirportData(destination.lower())
            middleware = Flightdata_Middleware()
            time_now = datetime.utcnow()

            numbers = response.xpath(r"//span[@class='flight-code']/text()").getall()
            deptime = response.xpath(r"//span[@class='flight-time departure-flight-time']/b[1]/text()").getall()
            arrtime = response.xpath(r"//span[@class='flight-time arrival-flight-time']/b[1]/text()").getall()

            airplane_type = response.xpath(r'//span[@class="aircraft-type"]/text()').getall()
            if not airplane_type:
                for num in range(len(numbers)):
                    airplane_type.append('Boeing 737')
            elif len(airplane_type) < len(numbers):
                for num in range(len(airplane_type),len(numbers)):
                    airplane_type.append('Boeing 737')
            num_seat = []
            for types in airplane_type:
                if(types is 'TigerAir A320'):
                    num_seat.append(180)
                else:
                    num_seat.append(186)
            departure_date = response.xpath(r"//li[@class='current-date']/text()").getall()
            if(departure_date):
                dep_day = departure_date[0].split(' ')[1]
                dep_month = strptime(departure_date[0].split(' ')[2], '%b').tm_mon
            else: #most likely no more flight on the current day
                dep_day = datetime.today().day
                dep_month = datetime.today().month

            #duration = response.xpath(r"//span[@class='duration hideExpanded']/text()").getall()
            if(dep_month < datetime.today().month):
                departure_year = (datetime.today() + timedelta(days=365)).year
            else:
                departure_year = datetime.today().year

            lightfare = []
            expressfare = []

            light = response.xpath(r"//td[@data-fare-type='Light Fare']/text()").getall()
            express = response.xpath(r"//td[@data-fare-type='Express Fare']/text()").getall()
            for i in range(len(light)):
                temp = re.findall(r'\w+', light[i]) 
                p = '.'.join(temp)
                if (p and p != 'Not.Available'):
                    lightfare.append(p)
                elif (p == 'Not.Available'):
                    lightfare.append('Sold out')
        
                temp = re.findall(r'\w+', express[i]) 
                p = '.'.join(temp)
                if (p and p != 'Not.Available'):
                    expressfare.append(p)
                elif (p == 'Not.Available'):
                    expressfare.append('Sold out')

        
            for i in range(len(numbers)):
                try:
                    flight_number = (" ".join(numbers[i].split())).replace(" ","")
                    departure_datetime = (datetime(departure_year, dep_month, int(dep_day),
                                        int(deptime[i].split(':')[0]), int(deptime[i].split(':')[1].split(' ')[0])))
                    plus_day = response.xpath(r"//span[@class='plusDays']/text()").getall()
                    if(plus_day):
                        arrival_datetime = (datetime(departure_year, dep_month, int(dep_day),
                                        int(arrtime[i].split(':')[0]), int(arrtime[i].split(':')[1].split(' ')[0])) + timedelta(days=1))
                    else:
                        arrival_datetime = (datetime(departure_year, dep_month, int(dep_day),
                                        int(arrtime[i].split(':')[0]), int(arrtime[i].split(':')[1].split(' ')[0])))

                    # add timezone to datetime
                    dep_time = TimezoneConverter().time_to_utc(origin.lower(), departure_datetime)
                    arr_time = TimezoneConverter().time_to_utc(destination.lower(), arrival_datetime)        
                
                    flight_detail = {
                        'departure_iata' : dep_airport_data.iata,
                        'arrival_iata' : dest_airport_data.iata,
                        'flight number' : flight_number,
                        'airplane type' : airplane_type[i],
                        'departure time' : dep_time,
                        'arrival time' : arr_time,
                        'lightfare' : lightfare[i],
                        'expressfare' : expressfare[i],
                    }
                    yield (flight_detail)
                    '''
                    #TO BE DONE in middleware integration
                    middleware.insert_flightinfo(flight_number=flight_number,
                                                 departure_time=dep_time,
                                                 arrival_time=arr_time,
                                                 origin=dep_airport_data.iata,
                                                 destination=dest_airport_data.iata,
                                                 airline='Tigerair',
                                                 make=airplane_type[i].split(' ')[0],
                                                 model=airplane_type[i].split(' ')[1]
                                                 )

                    middleware.insert_airfare(flight_number,
                                              time_now,
                                              'lightfare',
                                              'One Way',
                                              float(lightfare[i])
                                              )
                    middleware.insert_airfare(flight_number,
                                              time_now,
                                              'expressfare',
                                              'One Way',
                                              float(expressfare[i])
                                              )
                    '''
                    self.logScraping(timestamp, self.link, flight_number, "airfare", status)
                except(TypeError, AttributeError) as e:
                    print('Failed to scrape search result page')
                    print(e)
                    self.reportError(datetime.now(), self.link, 'error', e)
            self.onSessionEnd()
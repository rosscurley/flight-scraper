function main(splash, args)
    assert(splash:go{
        args.url,
        headers=args.headers,
        http_method=args.http_method,
        body=args.body,
      })
  
  	--oneway trip
    assert(splash:wait(0.5))
    local element = splash:select('.radio.radio-primary label:nth-child(2)')
    element:mouse_click()
    assert(splash:wait(0.5))
  
  	--departure and arrival
  	local dep_field = splash:select('#departurePortSelected')
  	dep_field:send_text(args.dep)
  	assert(splash:wait(1.5))
  	local arr_field = splash:select('#arrivalPortSelected')
  	arr_field:send_text(args.arr)
  	assert(splash:wait(1.5))
  	
  	--select date
  	element = splash:select("#departureDateField"):mouse_click()
  	assert(splash:wait(0.5))
  	
  	local select_date = splash:jsfunc([[
    	function(day, month){
    		var monthHeader = document.querySelector('span.ui-datepicker-month');
    		while (!monthHeader.firstChild.data.includes(month)){
    			document.querySelector("a[data-handler=next]").click();
    			var monthHeader = document.querySelector('span.ui-datepicker-month');
  			}
    document.querySelectorAll('.ui-state-default')[day-1].click()
    }
    ]])
  	select_date(args.day, args.month)
  	assert(splash:wait(0.5))
    
    element = splash:select('#searchFlightBtn'):mouse_click()
    assert(splash:wait(15))
    
    return {
      url = splash:url(),
      html = splash:html()
    }
  end
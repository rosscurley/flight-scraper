# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html
# items.py - tigerair.py

import scrapy
import general.scrapy_items as items
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, TakeFirst
import datetime

class FlightInfoLoader(ItemLoader):
    default_item_class=items.FlightInfo

    default_input_processor = Identity()
    default_output_processor = TakeFirst()

class FlightStatusLoader(ItemLoader):
    default_item_class=items.FlightStatus

    default_input_processor = Identity()
    default_output_processor = TakeFirst()

class AirfareLoader(ItemLoader):
    default_item_class=items.Airfare
    
    default_input_processor = Identity()
    default_output_processor = TakeFirst()
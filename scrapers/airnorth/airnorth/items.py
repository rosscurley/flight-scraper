# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import datetime

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose, Identity, TakeFirst

import general.scrapy_items as items
from general.timezones import TimezoneConverter

tzConverter = TimezoneConverter()

def parseTime(timestr, flightEnd, loader_context):
    parts = timestr[0].split(":")
    hours, mins =  int(parts[0]), int(parts[1])
    timestamp = datetime.datetime(loader_context["year"], loader_context["month"], loader_context["day"], hours, mins)
    timeStr = tzConverter.time_to_utc(loader_context[flightEnd], timestamp)
    return timeStr

def parseTimeOrigin(self, timestr, loader_context):
    return parseTime(timestr, "origin", loader_context)

def parseTimeDest(self, timestr, loader_context):
    return parseTime(timestr, "dest", loader_context)

class FlightInfoLoader(ItemLoader):
    default_item_class=items.FlightInfo

    departure_time_in = parseTimeOrigin
    arrival_time_in = parseTimeDest

    default_input_processor = Identity()
    default_output_processor = TakeFirst()


class FlightStatusLoader(ItemLoader):
    default_item_class=items.FlightStatus

    departure_time_in = parseTimeOrigin
    arrival_time_in = parseTimeDest

    default_input_processor = Identity()
    default_output_processor = TakeFirst()


def parse_fare(self, text):
    return text[0].strip(" $").replace(",","")

class AirfareLoader(ItemLoader):
    default_item_class=items.Airfare
    
    default_input_processor = Identity()
    default_output_processor = TakeFirst()

    fare_in = parse_fare

function fillValues(values, splash)
    prefix="ctl00$ContentPlaceHolder1$"
    values[prefix.."ddlOrigin"] = splash.args.origin
    values[prefix.."ddlDestination"] = splash.args.destination
    values[prefix.."ddlDepMonth"] = splash.args.month
    values[prefix.."ddlDepDay"] = splash.args.day
    values[prefix.."trip_direction"] = "rbOneWay"
end

function main(splash)
    splash:init_cookies(splash.args.cookies)
    assert(splash:go{
        splash.args.url,
        headers=splash.args.headers,
        http_method=splash.args.http_method,
        body=splash.args.body,
        })
    assert(splash:wait(3.0))

    local form = splash:select("#aspnetForm")
    if form == nil then
        local body = splash:select("body")
        local content = body:text()
        if string.find(content, "blocked") == nil then
            return {
                url = splash:url(),
                no_form=true,
                cause="unknown",
            }
        else
            return {
                url = splash:url(),
                no_form=true,
                cause="blocked",
            }
        end
    else
        local values = assert(form:form_values())
        fillValues(values, splash)
        form:fill(values)
        local values2 = assert(form:form_values())
        local newDest = values2["ctl00$ContentPlaceHolder1$ddlDestination"]


        
        if newDest == splash.args.destination then
            local sub = splash:select("#ctl00_ContentPlaceHolder1_btnContinue")
            assert(sub:mouse_click())
            splash:wait(5)
            local entries = splash:history()
            local last_response = entries[#entries].response
            return {
                url = splash:url(),
                headers = last_response.headers,
                http_status = last_response.status,
                html = splash:html(),
                png = splash:png(),
                dest_unavailable = false,
                no_form=false,
            }
        else
            return {
                url = splash:url(),
                headers = last_response.headers,
                http_status = last_response.status,
                no_form=false,
                dest_unavailable = true,
            }
        end
    end
end

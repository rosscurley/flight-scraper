# test_qantas.py - test script for the qantas scraper, runs through each route once and dumps scraped info to a json file
# Author - Jack Lardner
# Last Modified - 18/10/19
# License - https://www.gnu.org/licenses/gpl-3.0.en.html

from datetime import datetime

import scrapy
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging
from twisted.internet import defer, reactor

from qantas.spiders.qantas import QantasSpider

today = datetime.now()
travel_date = '{0},{1}'.format(today.day, today.month)
base_settings = get_project_settings()
base_settings['FEED_FORMAT'] = 'json'
configure_logging()

# routes that qantas covers
routes = [
    'perth sydney',
    'perth melbourne',
    'perth brisbane',
    'perth adelaide',
    'perth cairns',
    'brisbane sydney',
    'brisbane melbourne',
    'perth karratha',
    'kalgoorlie perth',
    'port_hedland perth',
    'newman perth',
    'geraldton perth',
    'broome perth',
    'kununurra perth',
    'learmonth perth',
    'broome kununurra'
]


@defer.inlineCallbacks
def crawl():
    # scrape each route both ways
    for route in routes:
        settings = base_settings
        dep = route.split(' ')[0]
        dest = route.split(' ')[1]
        runner = CrawlerRunner(settings)
        settings['FEED_URI'] = '{0}-{1}.json'.format(dep, dest)
        yield runner.crawl(QantasSpider, dep=dep, dest=dest, travel_date=travel_date, user_agent='random')
        settings['FEED_URI'] = '{0}-{1}.json'.format(dest, dep)
        yield runner.crawl(QantasSpider, dep=dest, dest=dep, travel_date=travel_date, user_agent='chrome')
    reactor.stop()


crawl()
reactor.run()

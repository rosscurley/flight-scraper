# qantas.py - Spider for qantas flight data
# Author - Jack Lardner
# Last modified - 28/10/19
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# To run this spider:
# scrapy crawl qantas -a dep=DEP -a dest=DEST -a travel_date=DD,MM -a user_agent=(chrome|random)
# to dump screenshots of what Splash renders add -a debug=True
# to output scraped flight data to a json format add -o flights.json
# this spider can take a very long time to run

import scrapy
from scrapy.selector import Selector
import base64
from scrapy_splash import SplashRequest
import calendar
from general.user_agents import user_agents
from general.logging_spider import LoggingSpider
from general.airport_data import AirportData
from general.timezones import TimezoneConverter
import os.path
from datetime import datetime
from scraper_middleware.flightdata_middleware import Flightdata_Middleware


class QantasSpider(LoggingSpider):
    name = 'qantas'

    # map of fare classes to css classes
    qantas_fare_classes = {
        'Red e-Deal': 'fare_AUAURED1JQ',
        'Flex': 'fare_AUAUFL2JQ',
        'Business': 'fare_AUAUBUS',
    }
    # List of airports
    airports = [
        'perth',
        'sydney',
        'melbourne',
        'brisbane',
        'adelaide',
        'cairns',
        'karratha',
        'kalgoorlie',
        'port_hedland',
        'newman',
        'geraldton',
        'broome',
        'kununurra',
        'learmonth'
    ]
    # link to Qantas homepage
    link = 'https://www.qantas.com/au/en.html'

    # init function to assign passed arguments
    def __init__(self, dest, dep, travel_date, user_agent, debug=False, *args, **kwargs):
        super(QantasSpider, self).__init__(*args, **kwargs)
        if dep not in self.airports or dest not in self.airports:
            raise ValueError('Airport not supported')
        self.dep = dep
        self.dest = dest
        self.travel_day = int(travel_date.split(',')[0])
        self.travel_month = int(travel_date.split(',')[1])
        self.user_agent = user_agents[user_agent]
        self.debug = debug
        # Lua script to control splash
        script_file = os.path.join(os.path.dirname(__file__), 'qantas_script.lua')
        with open(script_file) as file:
            self.lua_script = file.read()

    # Load homepage, input data and submit form
    def start_requests(self):
        # Arguments given to Splash
        splash_args = {
            'lua_source': self.lua_script,
            'images': 1,
            'timeout': 400,
            'user_agent': self.user_agent,
            'link': self.link,
            # script requires capitalized locations and no underscores
            'dep': self.dep.replace('_', ' ').capitalize(),
            'dest': self.dest.replace('_', ' ').capitalize(),
            'day': self.travel_day,
            # pass the month name rather than the month number
            'month': calendar.month_name[self.travel_month].lower(),
            'detail': False,
            'melbourne': False
        }
        # Load the Qantas homepage, enter information manually and submit
        yield SplashRequest(self.link,
                            endpoint='execute',
                            callback=self.parse_result_page,
                            cache_args=['lua_source'],
                            args=splash_args,
                            session_id="1"
                            )

    # Gathers flight numbers and fare prices
    # Other details are scraped by parse_detail_page function
    def parse_result_page(self, response):
        # Following block is responsible for creating PNG image of Splash data
        # Can be used to verify the page is rendering correctly
        if self.debug:
            print('Scraping result page')
            imgdata = base64.b64decode(response.data['png'])
            filename = 'qantas-' + self.dep + '-' + self.dest + '.png'
            with open(filename, 'wb') as f:
                f.write(imgdata)

        # Iterate through flight list, grab flight number and fare prices
        flights = []
        flight_numbers = []
        try:
            # Get all flights from the results page
            flight_data = response.xpath('//*[@class="row itinerary"]')
            for flight in flight_data:
                # check if flight has a layover, if not continue
                if(len(flight.xpath('.//upsell-segment-details').getall()) == 1 and
                   flight.xpath('.//*[@class="e2e-flight-number"]/text()').get().startswith('QF') and
                   self.dep.split('_')[0].capitalize() in flight.xpath('.//*[@class="textual-label"]/text()').getall()[0]):

                    flight_number = flight.xpath(
                        './/*[@class="e2e-flight-number"]/text()').get()
                    flight_numbers.append(flight_number)
                    # get the prices for all available classes
                    fares = {}
                    for fare_class in self.qantas_fare_classes:
                        # Following xpath statement finds
                        # price for the given css class
                        price = flight.xpath(
                            './/*[contains(@class, "' + self.qantas_fare_classes[fare_class] + '")]//*[@class="amount cash ng-star-inserted"]/text()'
                        ).get()
                        sold_out = flight.xpath('.//*[contains(@class, "' + self.qantas_fare_classes[fare_class] + '")]//*[@class="amount no-seat"]/text()').get()
                        if sold_out is not None:
                            price = sold_out
                        elif price is None:
                            price = "N/A"
                        fares[fare_class] = price
                    # Add gathered data from results page into flight list
                    flights.append({
                        'flight_number': flight_number,
                        'fares': fares
                    })

                # else remaining flights have layovers, skip
                elif (len(flight.xpath('.//upsell-segment-details').getall()) > 1):
                    break
        except(TypeError, AttributeError) as e:
            print('Failed to scrape search result page')
            print(e)
            # send error report to logs
            self.reportError(
                datetime.now(timezone.utc),
                self.link,
                'error',
                e
            )
        # do another fare run only for Melbourne business class flights
        if self.dep == 'melbourne' or self.dest == 'melbourne':
            yield SplashRequest(self.link,
                                callback=self.parse_melbourne_business,
                                cb_kwargs=dict(flights=flights),
                                endpoint='execute',
                                session_id='1',
                                cache_args=['lua_source'],
                                args={
                                    'lua_source': self.lua_script,
                                    'dest': self.dest.replace('_', ' ').capitalize(),
                                    'dep': self.dep.replace('_', ' ').capitalize(),
                                    'day': self.travel_day,
                                    'month': calendar.month_name[self.travel_month].lower(),
                                    'detail': False,
                                    'user_agent': self.user_agent,
                                    'images': 1,
                                    'timeout': 400,
                                    'cookies': response.data['cookies'],
                                    'link': self.link,
                                    'melbourne': True
                                })
        else:
            yield SplashRequest(self.link,
                                callback=self.parse_detail_page,
                                cb_kwargs=dict(flights=flights),
                                endpoint='execute',
                                session_id='1',
                                cache_args=['lua_source'],
                                args={
                                    'lua_source': self.lua_script,
                                    'dest': self.dest.replace('_', ' ').capitalize(),
                                    'dep': self.dep.replace('_', ' ').capitalize(),
                                    'day': self.travel_day,
                                    'month': calendar.month_name[self.travel_month].lower(),
                                    'detail': True,
                                    'melbourne': False,
                                    'user_agent': self.user_agent,
                                    'images': 1,
                                    'timeout': 400,
                                    'cookies': response.data['cookies'],
                                    'flight_numbers': flight_numbers,
                                    'link': self.link
                                })

    # Scrape all 'detail' pages for flights
    # Gathers times and aircraft model
    def parse_detail_page(self, response, flights):
        html_data = response.data['html_data']
        png_data = response.data['png_data']
        if self.debug:
            print('Scraping flight details')
            for flight in flights:
                # Writes image to file, used for debugging
                imgdata = base64.b64decode(png_data[flight['flight_number']])
                filename = 'detail-image-' + self.dep + '-' + self.dest + '-' + flight['flight_number'] + '.png'
                with open(filename, 'wb') as f:
                    f.write(imgdata)

        # get airport data classes
        dep_airport_data = AirportData(self.dep)
        dest_airport_data = AirportData(self.dest)
        middleware = Flightdata_Middleware()
        time_now = datetime.utcnow()
        # for each flight add detail data
        for flight in flights:
            # Transform html text into a scrapy selector for easier traversal
            html_selector = Selector(text=html_data[flight['flight_number']])
            # Isolate table with relevent data
            flight_html = html_selector.xpath('//*[@class="list-inline flight-info-details-panel col-12 col-lg-8"]/tbody')
            try:
                # extract dates and times
                departure_date = flight_html.xpath('.//tr[2]/td[2]/text()').get()
                departure_time = flight_html.xpath('.//tr[3]/td[2]/text()').get()
                arrival_date = flight_html.xpath('.//tr[5]/td[2]/text()').get()
                arrival_time = flight_html.xpath('.//tr[6]/td[2]/text()').get()

                # create datetime objects for departure and arrival
                departure_datetime = datetime(int(departure_date.split(' ')[3]), int(self.travel_month), int(self.travel_day),
                                              int(departure_time.split(':')[0]), int(departure_time.split(':')[1]))
                arrival_datetime = datetime(int(arrival_date.split(' ')[3]), int(self.travel_month), int(self.travel_day),
                                            int(arrival_time.split(':')[0]), int(arrival_time.split(':')[1]))

                # add timezone to datetime
                flight['dep_time'] = TimezoneConverter().time_to_utc(self.dep, departure_datetime)
                flight['arr_time'] = TimezoneConverter().time_to_utc(self.dest, arrival_datetime)

                flight['aircraft_type'] = flight_html.xpath('.//tr[10]/td[2]/text()').get()
                flight['departure_iata'] = dep_airport_data.iata
                flight['departure_location'] = dep_airport_data.location
                flight['destination_iata'] = dest_airport_data.iata
                flight['destination_location'] = dest_airport_data.location
                # insert aircraft info to db
                middleware.insert_aircraft(flight['aircraft_type'].split(' ')[0],
                                           ' '.join(flight['aircraft_type'].split(' ')[1:]),
                                           0)
                # insert flightinfo info to db
                middleware.insert_flightinfo(flight_number=flight['flight_number'],
                                             departure_time=flight['dep_time'],
                                             arrival_time=flight['arr_time'],
                                             origin=flight['departure_iata'],
                                             destination=flight['destination_iata'],
                                             airline='Qantas',
                                             make=flight['aircraft_type'].split(' ')[0],
                                             model=' '.join(flight['aircraft_type'].split(' ')[1:]))
                # insert fare info to db
                for fare in flight['fares']:
                    price = flight['fares'][fare].replace('$', '')
                    price = price.replace(',', '')
                    if price.isdigit():
                        middleware.insert_airfare(flight['flight_number'],
                                                  time_now,
                                                  fare,
                                                  'One Way',
                                                  int(price)
                                                  )
                # log status of scraped data for each flight
                self.logScraping(
                    datetime.utcnow(),
                    self.link,
                    flight['flight_number'],
                    'airfare'
                )
                yield(flight)
            # Detail page failed to render
            except (TypeError, AttributeError) as e:
                print('Failed to scrape detail page for flight ' + flight['flight_number'])
                print(e)
                # send error report to logs
                self.reportError(
                    datetime.now(timezone.utc),
                    self.link,
                    'error',
                    e
                )
        self.onSessionEnd()

    # Altered parse_result_page function
    # Only scrapes business class fares for Melbourne flights due to Qantas separating them in search results
    # After business class fares are collected and added to flight data it moves on to detailed scraping
    def parse_melbourne_business(self, response, flights):
        if self.debug:
            print('Scraping Melbourne business class fares')
            imgdata = base64.b64decode(response.data['png'])
            filename = 'qantas-' + self.dep + '-' + self.dest + '-business-class.png'
            with open(filename, 'wb') as f:
                f.write(imgdata)

        # Get all flights from the results page
        flight_data = response.xpath('//*[@class="row itinerary"]')

        flight_numbers = []
        business_class_data = {}
        for flight in flight_data:
            # check for layover, if not continue
            if(len(flight.xpath('.//upsell-segment-details').getall()) == 1 and
               flight.xpath('.//*[@class="e2e-flight-number"]/text()').get().startswith('QF') and
               flight.xpath('.//*[@class="textual-label"]/text()').getall()[0] == self.dep.replace('_', ' ').capitalize()):

                flight_number = flight.xpath(
                    './/*[@class="e2e-flight-number"]/text()').get()
                flight_numbers.append(flight_number)

                price = flight.xpath(
                    './/*[contains(@class, "' + self.qantas_fare_classes['Business'] + '")]//*[@class="amount cash ng-star-inserted"]/text()'
                ).get()
                sold_out = flight.xpath('.//*[contains(@class, "' + self.qantas_fare_classes['Business'] + '")]//*[@class="amount no-seat"]/text()').get()
                if sold_out is not None:
                    price = sold_out
                business_class_data[flight_number] = price

            # else remaining flights have layovers, skip
            elif (len(flight.xpath('.//upsell-segment-details').getall()) > 1):
                break

        # Combine business class fare data with the other fare data
        for flight in flights:
            flight['fares']['Business'] = business_class_data[flight['flight_number']]

        # Continue with detail scraping
        yield SplashRequest(self.link,
                            callback=self.parse_detail_page,
                            cb_kwargs=dict(flights=flights),
                            endpoint='execute',
                            session_id='1',
                            cache_args=['lua_source'],
                            args={
                                'lua_source': self.lua_script,
                                'dest': self.dest.replace('_', ' ').capitalize(),
                                'dep': self.dep.replace('_', ' ').capitalize(),
                                'day': self.travel_day,
                                'month': calendar.month_name[self.travel_month].lower(),
                                'detail': True,
                                'melbourne': False,
                                'user_agent': self.user_agent,
                                'images': 1,
                                'timeout': 400,
                                'cookies': response.data['cookies'],
                                'flight_numbers': flight_numbers,
                                'link': self.link
                            })

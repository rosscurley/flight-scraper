-- qantas_script.lua - Lua script for Splash to scrape flight information from Qantas
-- Author - Jack Lardner
-- Last modified - 05/10/19
-- License - https://www.gnu.org/licenses/gpl-3.0.en.html
-- Not designed to be run by itself, used by QantasSpider in qantas.py

function main(splash, args)
    function insert_text(field, char_length, text)
        for i = 0, char_length-1, 1
        do
            field:send_keys('<Backspace>')
            assert(splash:wait(0.1))
        end
        field:send_text(text)
        assert(splash:wait(0.5))
        field:send_keys('<Return>')
        assert(splash:wait(0.1))
    end

    splash:set_user_agent(args.user_agent)
    splash:init_cookies(args.cookies)
    assert(splash:go(args.link))
    assert(splash:wait(17))

    -- add data to form
    splash:runjs('document.querySelectorAll(".qfa1-radiobutton__label")[5].click()')
    assert(splash:wait(0.5))
    local dep_field = splash:select('#typeahead-input-from')
    local dest_field = splash:select('#typeahead-input-to')
    insert_text(dep_field, splash:evaljs('document.getElementById("typeahead-input-from").value.length'), args.dep)
    insert_text(dest_field, splash:evaljs('document.getElementById("typeahead-input-to").value.length'), args.dest)

    -- select departure date
    splash:select('#datepicker-input-departureDate'):mouse_click()
    local select_date = splash:jsfunc([[
        function(day, month) {
            // find the correct month
            var monthHeader = document.querySelector('div.date-picker__calendar:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > th:nth-child(1)');
            while (!monthHeader.firstChild.data.includes(month)){
                document.querySelector(".date-picker__arrow.date-picker__arrow-right.qfa1-arrow-icon").click();
                var monthHeader = document.querySelector('div.date-picker__calendar:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > th:nth-child(1)');
            }
            document.querySelectorAll('.date-picker__calendar-weekdays-items-text')[day-1].click();
        }
    ]])
    select_date(args.day, args.month)
    assert(splash:wait(3))

    -- set business class if melbourne flag is set
    if args.melbourne then
        local class_dropdown = splash:select('#select-input-travelClass')
        class_dropdown:mouse_click()
        assert(splash:wait(1))
        local business_class = splash:select('#select-picker-select-input-travelClass2')
        business_class:mouse_click()
        assert(splash:wait(1))
    end

    -- submit form
    local submit_button = splash:evaljs('document.querySelectorAll(".qfa1-submit-button__button")[1]')
    submit_button:mouse_click()
    assert(splash:wait(25))

    -- only run this code if we are scraping flight details
    if args.detail then
        -- scrape details for all flights at once
        flight_details_html = {}
        flight_details_png = {}
        local click_detail = splash:jsfunc([[
            function(flight_number) {
                flights = document.querySelectorAll('.e2e-flight-number');
                for(x = 0; x < flights.length; x++){
                    if(flights[x].innerHTML === flight_number)
                    {
                        return flights[x];
                    }
                }
            }
        ]])
        splash.scroll_position = {y=100}
        -- for each flight number, scrape its details
        for k, v in pairs(args.flight_numbers)
        do
            local detail = click_detail(v)
            assert(detail:mouse_click())
            assert(splash:wait(25))
            flight_details_html[v] = splash:html()
            flight_details_png[v] = splash:png()
            local close = splash:select('#full-flight-details-modal_cls')
            assert(close:mouse_click())
            assert(splash:wait(2))
        end
        return {
            html_data = flight_details_html,
            png_data = flight_details_png,
            cookies = splash:get_cookies()
        }
    end
    splash:set_viewport_full()
    return {
        html = splash:html(),
        png = splash:png(),
        cookies = splash:get_cookies()
    }
end
# virgin.py
# author : Ivandy Darmawan
# last modified: 1 Nov 2019
# scraper for virgin Australia
# it will only scrape the flight within Australia
# to run the scraper, use 'scrapy crawl virgin -a dep=<Departure> -a dest=<Destination> -a travel_date=<DD>,<MM>
# to output the result to a json file use -o <filename>.json
# NOTE: scraping the 60 days might end up getting the ip blocked

import base64
import calendar
from scrapy import Spider
from scrapy_splash import SplashRequest
from datetime import date
from datetime import timedelta
from datetime import datetime
from datetime import date
from time import strptime
import re
import os.path

#python_packages
from general.logging_spider import LoggingSpider
from general.user_agents import user_agents
from general.airport_data import AirportData
from general.timezones import TimezoneConverter
from scraper_middleware.flightdata_middleware import Flightdata_Middleware

class VirginSpider(LoggingSpider):
    name = 'virgin'
    link = r'https://www.virginaustralia.com/au/en/bookings/flights/make-a-booking/'

    fn = os.path.join(os.path.dirname(__file__), "virgin.lua")
    with open(fn) as file:
        script = file.read()

    # List of airports in Australia
    airports = [
        'Adelaide', 'Albury', 'Alice Springs', 'Ayers Rock',
        'Ballina', 'Brisbane', 'Broome', 'Bundaberg',
        'Cairns', 'Canberra', 'Christmas Island', 'Cloncurry', 'Coffs Harbour',
        'Darwin', 'Emerald', 'Geraldton', 'Gladstone', 'Gold Coast',
        'Hamilton Island', 'Hayman Island', 'Hervey Bay', 'Hobart', 
        'Kalgoorlie', 'Karratha', 'Kunurra', 'Launceston',
        'Mackay', 'Melbourne', 'Mildura', 'Moranbah', 'Mount Isa',
        'Newcastle', 'Newman', 'Onslow',
        'Paraburdoo', 'Perth', 'Port Hedland', 'Port Macquarie', 'Proserpine', 
        'Rockhampton', 'Sunshine Coast', 'Sydney', 'Tamworth', 'Townsville'
    ]

    def __init__(self, dep=None, dest=None, travel_date=None, user_agent='random', *args, **kwargs):
        super(VirginSpider, self).__init__(*args, **kwargs)
        if dep.lower().capitalize() not in self.airports or dest.lower().capitalize() not in self.airports:
            raise ValueError('Airport not supported')
        self.dep = dep.lower().capitalize()
        self.dest = dest.lower().capitalize()
        self.sixtyDays = False
        if travel_date is None:
            self.sixtyDays = True
        else:
            self.travel_day = int(travel_date.split(',')[0])
            self.travel_month = int(travel_date.split(',')[1])
            if(self.travel_month < datetime.now().month):
                self.scrape_date = datetime.today() + timedelta(days=365)
                self.scrape_date = datetime(self.scrape_date.year, self.travel_month, self.travel_day)
                #check if leap year
                if(calendar.isleap(self.scrape_date.year) and self.travel_month > 2):
                    self.scrape_date += timedelta(days=1)
            else:
                self.scrape_date = datetime(datetime.today().year, self.travel_month, self.travel_day)

        self.user_agent = user_agents[user_agent]

    def start_requests(self):
        if(self.sixtyDays):
            self.scrape_date = datetime(datetime.today().year, datetime.today().month, datetime.today().day)
            for i in range(60):
                print('============= ' + f'scraping {self.dep}-{self.dest} on {calendar.month_name[self.scrape_date.month]} {self.scrape_date.day}...')
                splash_args = {
                    'lua_source': self.script,
                    'html' : 1,
                    'images': 1,
                    'dep': self.dep,
                    'dest': self.dest,
                    'dates' : self.scrape_date.strftime('%d %b, %Y'),
                    'detail': False,
                    'timeout' : 90
                }

                yield SplashRequest(self.link,
                                    endpoint='execute',
                                    callback=self.parse_result_page,
                                    cache_args=['lua_source'],
                                    args=splash_args,
                                    session_id="1"
                                    )
                self.scrape_date += timedelta(days=1)
        else:
            print('============= ' + f'scraping {self.dep}-{self.dest} on {calendar.month_name[self.travel_month]} {self.travel_day}...')
            splash_args = {
                'lua_source': self.script,
                'html' : 1,
                'images': 1,
                'dep': self.dep,
                'dest': self.dest,
                'dates' : self.scrape_date.strftime('%d %b, %Y'),
                'detail': False,
                'timeout' : 90
            }

            yield SplashRequest(self.link,
                                endpoint='execute',
                                callback=self.parse_result_page,
                                cache_args=['lua_source'],
                                args=splash_args,
                                session_id="1"
                                )
    
    def parse_result_page(self, response):
        print(response.url)
        if '?error=login_required' in response.url:
            #log for error
            self.logScraping(datetime.utcnow(), self.link, "unknown", 'airfare', "failed", "Error while scraping the result page")
            self.reportError(datetime.utcnow(), self.link, 'error', 'Could not reach to the result page')
        middleware = Flightdata_Middleware()
        time_now = datetime.utcnow()
        status = "success"
        #gathers all data
        data_odd = response.xpath('//*[@class="yui-dt-odd"]')
        data_even = response.xpath('//*[@class="yui-dt-even"]')
        flight_data = []
        for i in data_even:
            flight_data.append(i)
        for i in data_odd:
            flight_data.append(i)

        #airport data
        dep_airport_data = AirportData(self.dep.lower())
        dest_airport_data = AirportData(self.dest.lower())

        for flights in flight_data:
                
            try:
                #flight numbers
                numbers = flights.xpath('.//*[@class="flight-number-and-direction"]/a/text()').getall()
                if(len(numbers) > 1): #transit flights
                    break
                else: #Non-stop flights
                    temp = re.findall(r'\w+', numbers[0])
                    flight_number = ''.join(temp)

                #times
                dep_year = self.scrape_date.year
                dep_month = self.scrape_date.month
                dep_day = self.scrape_date.day

                #departure time
                dep_time = flights.xpath('.//th[1]/span[2]/text()').get()
                arr_time = flights.xpath('.//th[2]/span[2]/text()').get()
                if('PM' in dep_time):
                    dep_hour = int(dep_time.split(':')[0])
                    if(dep_hour != 12):
                        dep_hour += 12
                    dep_min = int((dep_time.split(':')[1]).split(' ')[0])
                else:
                    dep_hour = int(dep_time.split(':')[0])
                    if(dep_hour == 12):
                        dep_hour -= 12
                    dep_min = int((dep_time.split(':')[1]).split(' ')[0])
            
                #arrival time
                if('PM' in arr_time):
                    arr_hour = int(arr_time.split(':')[0])
                    if(arr_hour != 12):
                        arr_hour += 12
                    arr_min = int((arr_time.split(':')[1]).split(' ')[0])
                else:
                    arr_hour = int(arr_time.split(':')[0])
                    if(arr_hour == 12):
                        arr_hour -= 12
                    arr_min = int((arr_time.split(':')[1]).split(' ')[0])

                departure_datetime = datetime(dep_year, dep_month, dep_day, dep_hour, dep_min)
                arrival_datetime = datetime(dep_year, dep_month, dep_day, arr_hour, arr_min)
                #check if arrival is the next day
                if(flights.xpath('.//*[@class="next-day-indicator next_day_indicator"]')):
                    arrival_datetime += timedelta(days=1)

                dep_time = TimezoneConverter().time_to_utc(self.dep.lower(), departure_datetime)
                arr_time = TimezoneConverter().time_to_utc(self.dest.lower(), arrival_datetime)

                #prices
                eco_price = flights.xpath('.//td[1]//span[@class="prices-amount"]/text()').get() 
                bus_price = flights.xpath('.//td[2]//span[@class="prices-amount"]/text()').get() 

                if(eco_price is None):
                    eco_price = 'Sold out'
                if(bus_price is None):
                    bus_price = 'Sold out'

                flight_detail = {
                    'departure_iata' : dep_airport_data.iata,
                    'arrival_iata' : dest_airport_data.iata,
                    'flight number' : flight_number,
                    'aircraft' : 'unknown type',
                    'departure time' : dep_time,
                    'arrival time' : arr_time,
                    'economy price' : eco_price,
                    'business price' : bus_price,
                }
                yield(flight_detail)
                middleware.insert_aircraft("unknown", "unknown", 0)

                middleware.insert_flightinfo(flight_number = flight_number,
                                             departure_time = dep_time,
                                             arrival_time = arr_time,
                                             origin= dep_airport_data.iata,
                                             destination = dest_airport_data.iata,
                                             airline = 'Virgin',
                                             make = "unknown",
                                             model = "unknown"
                )
                if eco_price is 'Sold out':
                    eco_price = 0
                if bus_price is 'Sold out':
                    bus_price = 0
                middleware.insert_airfare(flight_number, time_now, 'economy', 'One Way', float(eco_price or 0))
                middleware.insert_airfare(flight_number, time_now, 'business', 'One Way', float(bus_price or 0))

                self.logScraping(datetime.utcnow(), self.link, flight_number, 'airfare', status)
            except (TypeError, AttributeError) as e:
                print('Failed to scrape result page')
                print(e)
                self.logScraping(datetime.utcnow(), self.link, "unknown", 'airfare', "failed", "Error while scraping the result page")
                self.reportError(datetime.utcnow(), self.link, 'error', e)
        self.onSessionEnd()
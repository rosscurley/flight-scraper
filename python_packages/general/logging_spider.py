
from datetime import datetime
from urllib.parse import urlparse
import uuid
import scrapy

from scraper_middleware.logging_middleware import Logging_Middleware

from .data_models import ScraperError, ScraperLog, ScraperSession

sessionErrorsMap = {
    "fatal": "f",
    "recoverable": "r",
    "warning": "w",
    "none": "n",
}

logStatusMap = {
    "success": "S",
    "warning": "W",
    "error": "E",
    "failed": "F",
}

dataMap = {
    "airfare": "A",
    "flight status": "F",
}

errorSeverityMap = {
    "fatal": "f",
    "error": "E",
    "recoverable": "r",
    "warning": "w",
}

class LoggingSpider(scrapy.Spider):
    """
    Spider subclass with methods to keep track of logged info to be stored in the database.
    Make your spiders a subclass of this.
    In the constructor, make sure you pass the IP address of the scraper into the `ipAddress` keyword argument.
        This can be done by passing `-a ipAddress=<ip address>` as an argument when you run your scraper, as long as you pass **kwargs to super()__init__()

    When your scraper finishes scraping a page of results, call the `logScraping()` method to report how it went.
    If your scraper encounters an error, call the `reportError()` method to report it.
    When your scraper finishes (ie: in the `closed()` method), call the `onSessionEnd()` method.
    """

    def __init__(self, *args, ipAddress="127.0.0.1", **kwargs):
        super().__init__(*args, **kwargs)
        self.log_middleware = Logging_Middleware()
        self.sessionStart = datetime.utcnow()
        url = urlparse(self.link)
        self.targetSite = url.netloc
        self.logs = []
        self.errorLogs = []
        self.errors = "none"
        self.ipAddress = ipAddress

    def onSessionEnd(self):
        """
        Should be called when the scraper has finished scraping all the sites it needs to
        """
        sessionEnd = datetime.utcnow()
        scraperSession = {
            "sessionStart": self.sessionStart,
            "sessionEnd": sessionEnd,
            "targetSite": self.targetSite,
            "errors": self.errors,
            "scraperIP": self.ipAddress
        }
        self.scraperSession = ScraperSession.createDict(**scraperSession)
        self.insertIntoDatabase()

    def logScraping(self, timestamp, targetURL, flightNumber, data, status="success", message=None):
        """
        Should be called after each URL scraped, with the details of how the scraping went.

        timestamp should (preferably) be the value from the header of the HTTP response.  
        data should be either `airfare` or `flight status`  
        status should be one of the values listed on the wiki page for the database schema.  
        message is optional.
        """
        log = {
            "targetSite": self.targetSite,
            "sessionStart": self.sessionStart,
            "timestamp": timestamp,
            "targetURL": targetURL,
            "flightNumber": flightNumber,
            "data": data,
            "message": "" if message is None else message,
            "status": status,
        }
        self.logs.append(ScraperLog.createDict(**log))

    def reportError(self, timestamp, targetURL, severity, errorDetails):
        """
        Should be called whenever an error occurs, even if the scraper is able to recover.

        timestamp should (preferably) be the value from the header of the HTTP response.  
        severity should be one of the values listed on the wiki page for the database schema.  
        """
        err = {
            "timestamp": timestamp,
            "targetURL": targetURL,
            "severity": severity,
            "errorDetails": errorDetails,
        }
        self.errorLogs.append(ScraperError.createDict(**err))
        self.changeErrorLevel(severity)

    def changeErrorLevel(self, newError):
        """
        Used to change self.errors to a higher level if needed, based on the severity of a reported error.  
        Will do nothing if the severity of the reported error is lower than a previously reported error.
        """
        if self.errors == "fatal":
            pass  # nothing can be worse than fatal
        elif self.errors == "recoverable":
            if newError == "fatal":
                self.errors = "fatal"
        elif self.errors == "warning":
            if newError == "fatal":
                self.errors = "fatal"
            elif newError == "error":
                self.errors = "recoverable"
        elif self.errors == "none":
            if newError == "fatal":
                self.errors = "fatal"
            elif newError == "error":
                self.errors = "recoverable"
            elif newError == "recoverable" or newError == "warning":
                self.errors = "warning"

    def insertIntoDatabase(self):
        self.log_middleware.insert_session(
            self.scraperSession["targetSite"],
            self.scraperSession["sessionStart"],
            self.scraperSession["sessionEnd"],
            sessionErrorsMap[self.scraperSession["errors"]],
            self.scraperSession["scraperIP"]
        )
        for log in self.logs:
            self.log_middleware.insert_log(
                log["timestamp"],
                log["targetURL"],
                log["targetSite"],
                log["sessionStart"],
                log["flightNumber"],
                dataMap[log["data"]],
                log["message"],
                logStatusMap[log["status"]],
            )
        for err in self.errorLogs:
            self.log_middleware.insert_error(
                uuid.uuid4().hex,
                err["timestamp"],
                err["targetURL"],
                errorSeverityMap[err["severity"]],
                err["errorDetails"]
            )

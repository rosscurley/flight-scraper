import scrapy

class FlightInfo(scrapy.Item):
    flight_number = scrapy.Field()
    departure_time = scrapy.Field()
    arrival_time = scrapy.Field()
    origin = scrapy.Field()
    destination = scrapy.Field()
    airline = scrapy.Field()
    make = scrapy.Field()
    model = scrapy.Field()
    timeCollected = scrapy.Field()


class FlightStatus(scrapy.Item):
    flight_id = scrapy.Field()
    departure_time = scrapy.Field()
    departure_status = scrapy.Field()
    arrival_time = scrapy.Field()
    arrival_status = scrapy.Field()
    time_collected = scrapy.Field()

class Airfare(scrapy.Item):
    flight = scrapy.Field()
    fare_class = scrapy.Field()
    fare_type = scrapy.Field()
    fare = scrapy.Field()
    time_collected = scrapy.Field()

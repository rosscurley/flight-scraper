# airport_data.py - Contains data structures for common information on airports
# Author - Jack Lardner
# Last Modified - 14/10/19
# License - https://www.gnu.org/licenses/gpl-3.0.en.html


# AirportData - Contains IATA codes, latitudes and longitudes for a given airport
class AirportData:
    # codes gathered from https://www.iata.org/publications/pages/code-search.aspx (accessed 5/10/19)
    iata_codes = {
        'perth': 'PER',
        'sydney': 'SYD',
        'melbourne': 'MEL',
        'adelaide': 'ADL',
        'brisbane': 'BNE',
        'cairns': 'CNS',
        'karratha': 'KTA',
        'kalgoorlie': 'KGI',
        'port_hedland': 'PHE',
        'newman': 'ZNE',
        'geraldton': 'GET',
        'broome': 'BME',
        'kununurra': 'KNX',
        'learmonth': 'LEA',
        'carnarvon': 'CVQ',
        'esperance': 'EPR',
        'albany': 'ALH',
        'shark_bay': 'MJK',
        'mount_magnet': 'MMG',
        'meekatharra': 'MKR',
    }

    # airport name data taken from https://openflights.org/html/apsearch (accessed 7/10/19)
    airport_names = {
        'perth': 'Perth International Airport',
        'sydney': 'Sydney Kingsford Smith International Airport',
        'melbourne': 'Melbourne International Airport',
        'adelaide': 'Adelaide International Airport',
        'brisbane': 'Brisbane International Airport',
        'cairns': 'Cairns International Airport',
        'karratha': 'Karratha Airport',
        'kalgoorlie': 'Kalgoorlie Boulder Airport',
        'port_hedland': 'Port Hedland International Airport',
        'newman': 'Newman Airport',
        'geraldton': 'Geraldton Airport',
        'broome': 'Broome International Airport',
        'kununurra': 'Kununurra Airport',
        'learmonth': 'Learmonth Airport',
        'carnarvon': 'Carnarvon Airport',
        'esperance': 'Esperance Airport',
        'albany': 'Albany Airport',
        'shark_bay': 'Shark Bay Airport',
        'mount_magnet': 'Mount Magnet Airport',
        'meekatharra': 'Meekatharra Airport',
    }

    # latitude and longitude data was taken from https://openflights.org/html/apsearch (accessed 5/10/19)
    # broome location data was slightly inaccurate so data was taken from a point on google maps
    airport_latitude = {
        'perth': -31.94029998779297,
        'sydney': -33.94609832763672,
        'melbourne': -37.673302,
        'brisbane': -27.384199142456055,
        'adelaide': -34.945,
        'cairns': -16.885799408,
        'karratha': -20.712200164799995,
        'kalgoorlie': -30.789400100699996,
        'port_hedland': -20.3777999878,
        'newman': -23.417800903299998,
        'geraldton': -28.796101,
        'broome': -17.9506085,
        'kununurra': -15.7781000137,
        'learmonth': -22.235599517799997,
        'carnarvon': -24.880211,
        'esperance': -33.684399,
        'albany': -34.94329833984375,
        'shark_bay': -25.8938999176,
        'mount_magnet': -28.116100311279297,
        'meekatharra': -26.6117000579834,
    }

    airport_longitude = {
        'perth': 115.96700286865234,
        'sydney': 151.177001953125,
        'melbourne': 144.843002,
        'brisbane': 153.11700439453125,
        'adelaide': 138.531006,
        'cairns': 145.755004883,
        'karratha': 116.773002625,
        'kalgoorlie': 121.461997986,
        'port_hedland': 118.625999451,
        'newman': 119.803001404,
        'geraldton': 114.707001,
        'broome': 122.2314674,
        'kununurra': 128.707992554,
        'learmonth': 114.088996887,
        'carnarvon': 113.67174,
        'esperance': 121.822998,
        'albany': 117.80899810791016,
        'shark_bay': 113.577003479,
        'mount_magnet': 117.84200286865234,
        'meekatharra': 118.5479965209961,
    }

    def __init__(self, airport_name=None, iata=None):
        # if iata code is specified
        if iata is not None:
            for name in self.iata_codes.keys():
                if self.iata_codes[name] == iata:
                    key = name
                    break
        # else if airport name is specified
        elif airport_name is not None:
            key = airport_name
        # else both are None and an error must be thrown
        else:
            raise(ValueError('airport_name or iata must be specified'))
        self.key = key
        self.name = self.airport_names[key]
        self.iata = self.iata_codes[key]
        self.latitude = self.airport_latitude[key]
        self.longitude = self.airport_longitude[key]
        self.location = {
            'latitude': self.latitude,
            'longitude': self.longitude
        }

    def __eq__(self, other):
        if isinstance(other, AirportData):
            if (self.name == other.name and
               self.iata == other.iata and
               self.longitude == other.longitude and
               self.latitude == other.latitude):
                return True
            else:
                return False

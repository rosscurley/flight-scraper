# timezones.py - Contains TimezoneConverter class scrapers can use to format scraped times
# Author - Jack Lardner
# Last Modified - 09/10/19
# License - https://www.gnu.org/licenses/gpl-3.0.en.html
# eg. to grab a timezone string - TimezoneConverter().add_timezone('sydney', datetime(YYYY, MM, DD, HH, MM))

from datetime import datetime
import pytz


class TimezoneConverter:
    # collections for locations and their timezones
    timezones = {
        'perth': 'Australia/Perth',
        'sydney': 'Australia/Sydney',
        'melbourne': 'Australia/Melbourne',
        'brisbane': 'Australia/Brisbane',
        'adelaide': 'Australia/Adelaide',
        'cairns': 'Australia/Queensland',
    }

    # if a location is in this collection, its timezone is 'Australia/West'
    wa_region = [
        'karratha',
        'kalgoorlie',
        'port_hedland',
        'newman',
        'geraldton',
        'broome',
        'kununurra',
        'learmonth',
        'carnarvon',
        'esperance',
        'albany',
        'shark_bay',
        'mount_magnet',
        'meekatharra',
    ]

    def __init__(self):
        self.timezone_fmt_str = '%Y-%m-%d %H:%M:%S %Z%z'
        self.utc_fmt_str = '%Y-%m-%d %H:%M:%S %Z'

    # determine timezone for a location
    def get_timezone_code(self, location):
        if location in self.timezones:
            return self.timezones[location]
        elif location in self.wa_region:
            return 'Australia/West'
        else:
            raise(ValueError(f'Location: {location} not supported'))

    # given a location and a datetime, add timezone to datetime
    # return as a formatted string with timezone info and UTC offset
    def add_timezone(self, location, dt):
        tz = pytz.timezone(self.get_timezone_code(location))
        return tz.localize(dt).strftime(self.timezone_fmt_str)

    # given a location and a datetime, add timezone to datetime and convert to UTC
    # return as a formatted string with timezone info
    def time_to_utc(self, location, dt):
        tz = pytz.timezone(self.get_timezone_code(location))
        return tz.localize(dt).astimezone(pytz.UTC).strftime(self.utc_fmt_str)

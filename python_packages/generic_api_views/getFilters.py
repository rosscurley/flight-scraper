"""
Classes and methods to parse the query parameters to get the filters to be applied to the database query.
"""
from django.core.exceptions import FieldDoesNotExist
from django.http import HttpRequest
from django.db.models import Model, Field, QuerySet


from .common import (CheckParam, HasFieldNameMap, HasModelField,
                     fieldInternalTypeMap, convertToFieldType)


class UnknownFilterExpression(Exception):
    pass


class UnknownFieldException(Exception):
    pass


class OtherQueryParam(Exception):
    pass


class UnsupportedExpression(Exception):
    pass


# which fields are considered string fields
textFieldTypes = [
    "CharField",
    "TextField",
    "URLField",
    "IPAddressField",
]

# Expressions that can only be used for text fields
textOnlyExprs = [
    "iexact",
    "contains",
    "icontains",
    "startswith",
    "istartswith",
    "endswith",
    "iendswith",
]

# Expressions that cannot be used with text fields
textExcludedExprs = [
    "gte",
    "gt",
    "lt",
    "lte",
    "within",
    "range",
    "date",
]

# Simple expressions: those which only accept one value
simpleExpressions = [
    "exact",
    "iexact",
    "gte",
    "gt",
    "lt",
    "lte",
    "contains",
    "icontains",
    "startswith",
    "istartswith",
    "endswith",
    "iendswith",
]


def simpleExpressionParse(intFieldName: str, val: str, filtExpr: str, fieldClass: Field) -> (str, object):
    """
    Parses the values given to a `simple` expression
    """
    argVal = convertToFieldType(val, fieldClass)
    argName = f"{intFieldName}__{filtExpr}"
    return argName, argVal


def withinExpressionParse(intFieldName: str, val: list, filtExpr: str, fieldClass: Field) -> (str, object):
    """
    Parses the values given to a `within` expression
    """
    value = convertToFieldType(val[0], fieldClass)
    tol = convertToFieldType(val[1], fieldClass)
    argName = f"{intFieldName}__range"
    argVal = (value-tol, value+tol)
    return argName, argVal


def rangeExpressionParse(intFieldName: str, val: list, filtExpr: str, fieldClass: Field) -> (str, object):
    """
    Parses the values given to a `range` expression
    """
    low = convertToFieldType(val[0], fieldClass)
    high = convertToFieldType(val[1], fieldClass)
    argName = f"{intFieldName}__range"
    argVal = (low, high)
    return argName, argVal


def inExpressionParse(intFieldName: str, val: list, filtExpr: str, fieldClass: Field) -> (str, object):
    """
    Parses the values given to a `in` expression
    """
    vals = map(lambda x: convertToFieldType(x, fieldClass), val)
    valList = [ii for ii in vals]
    argName = f"{intFieldName}__in"
    return argName, valList


def dateExpressionParse(intFieldName: str, val: list, filtExpr: list, fieldClass: Field) -> (str, object):
    """
    Parses the values given to a `date` expression
    """
    prefix = f"{intFieldName}__date"
    argName, argVal = parseFilterExpression(
        prefix, val, filtExpr[1:], fieldClass)
    return argName, argVal


"""
Complex Expressions are those which accept more than one value.

This dict maps those expressions to the functions which handle them
"""
complexExpressionMap = {
    "within": withinExpressionParse,
    "range": rangeExpressionParse,
    "date": dateExpressionParse,
    "in": inExpressionParse,
}


def parseFilterExpression(intFieldName: str, val: list, filtExpr: list, fieldClass: Field) -> (str, object):
    """
    Main method for parsing the filter expressions and the values passed to them
    """
    if fieldClass.name in textFieldTypes:
        if filtExpr[0] in textExcludedExprs:
            raise UnsupportedExpression(
                f"{filtExpr[0]} is not supported for {fieldClass.name} fields")
    else:
        if filtExpr[0] in textOnlyExprs:
            raise UnsupportedExpression(
                f"{filtExpr[0]} is not supported for {fieldClass.name} fields")
    if filtExpr[0] in simpleExpressions:
        argName, argVal = simpleExpressionParse(
            intFieldName, val[0], filtExpr[0], fieldClass)
    elif filtExpr[0] in complexExpressionMap:
        argName, argVal = complexExpressionMap[filtExpr[0]](
            intFieldName, val, filtExpr, fieldClass)
    else:
        raise UnknownFilterExpression(
            f"Unknown filtering expression given: {'__'.join(filtExpr)}")
    return argName, argVal


class FilterParams(HasFieldNameMap, HasModelField, CheckParam):
    """
    Class that adds support for accepting 'filter expressions' as query parameters
    """

    def isParamValid(self, param: str) -> bool:
        parts = param.split("__")
        if parts[0] in self.fieldNameMap:
            if len(parts) == 2:
                if (parts[1] in simpleExpressions or parts[1] in complexExpressionMap) and parts[1] != "date":
                    return True
            elif len(parts) == 3:
                if (parts[2] in simpleExpressions or parts[2] in complexExpressionMap) and parts[2] != "date" and parts[1] == "date":
                    return True
        return super().isParamValid(param)

    def getFieldNameType(self, fieldName: str) -> (str, Field):
        """
        Gets the internal field name and the field class from the shortened name in the query string

        Throws an error if the field is not found
        """
        try:
            intFieldName = self.fieldNameMap[fieldName]
            fieldClass = self.model._meta.get_field(intFieldName)
        except KeyError as err:
            raise UnknownFieldException(f"Field not recognised: {fieldName}")
        except FieldDoesNotExist as err:
            raise UnknownFieldException(
                f"Field not found on the model: {fieldName}")
        except:
            raise UnknownFieldException(
                f"Unknown error trying to find field: {fieldName}")
        return intFieldName, fieldClass

    def getFilter(self, request: HttpRequest) -> dict:
        """
        Parses the query parameters and returns an obejct that can be used to filter a QuerySet.  
        It can be passed to the filter method by unpacking it, ie:  
        `filter(**out)`

        """
        qParams = request.GET
        out = {}
        for query in qParams:
            parts = query.split("__")
            try:
                intFieldName, fieldClass = self.getFieldNameType(parts[0])
            except:
                if self.isParamValid(query):
                    continue
                else:
                    continue
            try:
                argName, argVal = parseFilterExpression(
                    intFieldName, qParams.getlist(query), parts[1:], fieldClass)
            except UnsupportedExpression as err:
                raise err
            except UnknownFilterExpression as err:
                raise err
            except BaseException as err:
                raise err
            out[argName] = argVal
        return out

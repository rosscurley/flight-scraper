
from django.http import HttpRequest, HttpResponse, JsonResponse, StreamingHttpResponse
from django.views import View
from django.db.models import Model, Field, QuerySet

from .getAPI import GetAPIView

import csv

class DummyFile():
    def write(self, value):
        return value



def csvStreamGenerator(items: QuerySet):
    iterator = items.iterator()
    firstEntry: dict = next(iterator)
    fieldNames = [key for key in firstEntry]
    writer = csv.DictWriter(DummyFile(), fieldNames, quoting=csv.QUOTE_NONNUMERIC)
    yield writer.writerow({key:key for key in fieldNames})
    yield writer.writerow(firstEntry)
    for entry in iterator:
        yield writer.writerow(entry)



class CSVGetAPIView(GetAPIView):
    def getCsvResponse(self, request: HttpRequest):
        items = self.getData(request)
        response = StreamingHttpResponse(csvStreamGenerator(items), content_type="text/csv")
        path = request.path
        path = path.rstrip(r'/')
        endPart = path.split(r'/')[-1]
        response['Content-Disposition'] = f'attachment; filename="{endPart}.csv"'
        return response

    def get(self, request: HttpRequest, fmt=None, **kwargs):
        if fmt == "csv":
            return self.getCsvResponse(request)
        else:
            return super().get(request, fmt=fmt, **kwargs)

    


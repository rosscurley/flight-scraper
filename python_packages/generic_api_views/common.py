"""
Contains various classes, functions and constants used by many of the modules in this package
"""
from django.db.models import Model, Field, QuerySet
from isodate import parse_datetime
from uuid import UUID
from django.http import HttpRequest, QueryDict
from django.db.models import Model
import json


"""
Maps the internal type of a field (the one returned by Field.get_internal_type()) to a function that can convert strings to a type that can be used in the database
"""
fieldInternalTypeMap = {
    "BooleanField": bool,
    "CharField": str,
    "DateField": parse_datetime,
    "DateTimeField": parse_datetime,
    "DecimalField": float,
    "FloatField": float,
    "IntegerField": int,
    "IPAddressField": str,
    "PositiveSmallIntegerField": int,
    "TextField": str,
    "URLField": str,
    "UUIDField": UUID,
}

class FKeyError(Exception):
    pass


def getFKeyVal(argVal: str, argField: Field):
    model: Model = argField.related_model
    qSet: QuerySet = model.objects.filter(pk=argVal)
    num = qSet.count()
    if num<1:
        raise FKeyError(f"Couldn't find entry with PK {argVal} for foreign key field {argField.name}")
    elif num>1:
        raise FKeyError(f"Found multiple entries with PK {argVal} for foreign key field {argField.name}")
    else:
        return qSet.get()



def convertToFieldType(argVal: str, argField: Field):
    if argField.is_relation:
        outVal = getFKeyVal(argVal, argField)
    else:
        outVal = fieldInternalTypeMap[argField.get_internal_type()](argVal)
    return outVal

class HasFieldNameMap():
    """
    Specifies that a class has a `fieldNameMap` class field that subclasses can use.
    """

    """
    Subclasses should define this dict, such that it maps the field names it expects in the query string 
    to the field names used in the model.
    """
    fieldNameMap = {}


class HasModelField():
    """
    Specifies that a class has a `model` class field that subclasses can use.

    This must be an instance of a django `Model`
    """
    model: Model = None


class CheckParam():
    """
    Specifies that a class has a method to check if a filter param is valid.
    """

    def isParamValid(self, param: str) -> bool:
        """
        Checks whether `param` is a valid query parameter.
        Subclasses should either return true, or call this method on their superclass.
        """
        return False


class UnsupportedContentType(Exception):
    pass


class NoRequestBody(Exception):
    pass


def handleRequestBody(request: HttpRequest) -> dict:
    """
    Converts the request body for a POST/PATCH/PUT/DELETE request to a dict, 
    because django is really bad at handling request bodies...

    Throws errors if the request isn't POST/PATCH/PUT/DELETE or if the content-type isn't json or urlencoded form
    """
    if request.method in ["POST", "PATCH", "PUT", "DELETE"]:
        cType = request.headers["content-type"]
        if cType == "application/x-www-form-urlencoded":
            if request.method == "POST":
                rawData = request.POST
            else:
                rawData = QueryDict(request.body)
            data = {}
            for key in rawData:
                val = rawData.getlist(key)
                if len(val) == 1:
                    val = val[0]
                data[key] = val
        elif cType == "application/json":
            data = json.loads(request.body)
        else:
            raise UnsupportedContentType(f"Unsupported content type: {cType}")
        return data
    else:
        raise NoRequestBody(
            f"Cannot get request body for {request.method} request")

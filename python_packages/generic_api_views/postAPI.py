"""
Defines classes used to create a generic POST request API View
"""
from django.http import HttpRequest, HttpResponse
from django.views import View

from .common import HasFieldNameMap, HasModelField, handleRequestBody, fieldInternalTypeMap


class InvalidType(Exception):
    pass


class MissingField(Exception):
    pass


class PostAPIView(HasFieldNameMap, HasModelField, View):
    """
    A View with a generic POST request handler
    """

    # fields that are safe to be omitted when making a new entry
    optionalFields = []

    def sanitizeData(self, data: dict) -> dict:
        """
        Checks the request data to see if it is valid, and also converts the field names used in the request body to internal names.

        Throws errors if the data is not valid
        """
        newData = {}
        for field in self.fieldNameMap:
            if field in data:
                val = data[field]
                if isinstance(val, (list, dict)):
                    raise InvalidType(f"Array or object found for {field}, only scalar values are allowed")
                try:
                    fieldClass = self.model._meta.get_field(
                        self.fieldNameMap[field]).get_internal_type()
                    converter = fieldInternalTypeMap[fieldClass]
                    val = converter(val)
                except:
                    raise InvalidType(
                        f"Unable to convert {data[field]} to required type")
                newData[self.fieldNameMap[field]] = val
            elif field in self.optionalFields:
                pass
            else:
                raise MissingField(
                    f"Could not find field: {field} in request body")
        return newData

    def post(self, request: HttpRequest, fmt=None, **kwargs):
        """
        Handler for POST requests
        """
        data = handleRequestBody(request)
        data = self.sanitizeData(data)
        # TODO: handle IntegrityError if entry already exists
        self.model.objects.create(**data)
        return HttpResponse(status=204)

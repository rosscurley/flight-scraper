"""
Classes and methods to create Views for tables with a temporal model; 
ie: they have two fields that define the time period their values apply for.
"""
import datetime

from django.db.models import Model, QuerySet, Field
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.views import View

from .getAPI import GetAPIView
from .getFilters import FilterParams
from .common import (CheckParam, HasFieldNameMap, HasModelField,
                     handleRequestBody, FKeyError)

from isodate import parse_datetime

class InconsistentTemporalBody(Exception):
    pass


class TemporalFieldTypes:
    """
    Subclasses should define these two class fields, which identify which fields can uniquely identify an entry, besides their start time.
    """
    # Fields in the model used to identify an entry (other than the start time). Should be the shortened version used as the key in self.fieldNameMap
    pkFields = []
    # Other fields present in the model
    otherFields = []


class TemporalRangeFields:
    """
    Subclasses should define these two class fields, which identify which two fields define the time period the entry applies to.
    """
    # These fields should be the names of the fields that indicate the start and the end of the settings time range respectively. Should be the internal field name
    startInternalField = None
    endInternalField = None


class TemporalFilterParams(TemporalRangeFields, FilterParams):
    """
    Adds support for the "current" and "atTime" query parameters, which allow for querying temporal models at a specific point in time
    """

    def isParamValid(self, param: str):
        if param == "current" or param == "atTime":
            return True
        else:
            return super().isParamValid(param)

    def addToFilter(self, dt: datetime.datetime, filterArgs: dict):
        """
        Method for adding the needed fields to the filter argument
        """
        filterArgs[f"{self.startInternalField}__lte"] = dt
        filterArgs[f"{self.endInternalField}__gte"] = dt

    def getFilter(self, request: HttpRequest) -> dict:
        """
        Extends FilterParams.getFilter, adding the neccesary filters for `current` and `atTime`
        """
        out = super().getFilter(request)
        qParams = request.GET
        if "current" in qParams:
            timestamp = datetime.datetime.now(datetime.timezone.utc)
            self.addToFilter(timestamp, out)
        elif "atTime" in qParams:
            try:
                timestamp = parse_datetime(qParams.get("atTime"))
                self.addToFilter(timestamp, out)
            except:  # TODO: should this throw an error?
                pass
        return out


class TemporalHelperMethods(TemporalFieldTypes, TemporalRangeFields, HasFieldNameMap, HasModelField):
    """
    Contains helper methods used by multiple of the Temporal API View classes
    """

    def validateData(self, data: dict) -> int:
        """
        Checks that the data received is valid. 
        If it is valid, returns the number of entries in the request data

        Will throw an error if one of the fields is missing, or if not all fields have the same length.
        """
        if self.pkFields[0] in data:
            firstPk = data[self.pkFields[0]]
            if isinstance(firstPk, list):
                length = len(firstPk)
            else:
                length = 1
            for pk in self.pkFields:
                if pk in data:
                    pkVal = data[pk]
                    if isinstance(pkVal, list):
                        thisLen = len(pkVal)
                    else:
                        thisLen = 1
                    if thisLen != length:
                        raise InconsistentTemporalBody(
                            f"Field {pk} has length {thisLen}, expected {length}")
                else:
                    raise InconsistentTemporalBody(
                        f"Didn't find field {pk} in the request body")
            for field in self.otherFields:
                if field in data:
                    val = data[field]
                    if isinstance(val, list):
                        thisLen = len(val)
                    else:
                        thisLen = 1
                    if thisLen != length:
                        raise InconsistentTemporalBody(
                            f"Field {field} has length {thisLen}, expected {length}")
                else:
                    raise InconsistentTemporalBody(
                        f"Didn't find field {field} in the request body")
            return length
        else:
            raise InconsistentTemporalBody(
                f"Didn't find field {self.pkFields[0]} in the request body")

    def resolveFKeys(self, field: str, val: str):
        fieldClass: Field = self.model._meta.get_field(self.fieldNameMap[field])
        if fieldClass.is_relation:
            qSet = fieldClass.related_model.objects.filter(pk=val)
            num = qSet.count()
            if num<1:
                raise FKeyError(f"Couldn't find entry with PK {val} for foreign key field {fieldClass.name}")
            elif num>1:
                raise FKeyError(f"Found multiple entries with PK {val} for foreign key field {fieldClass.name}")
            else:
                return qSet.get()
        else:
            return val


    def getEntries(self, request: HttpRequest) -> list:
        """
        Gets the entries to be added to the table from the request data
        """
        data = handleRequestBody(request)
        entries = []
        length = self.validateData(data)
        if length == 1:
            newEntry = {}
            for field in self.pkFields:
                newEntry[self.fieldNameMap[field]] = self.resolveFKeys(field, data[field])
            for field in self.otherFields:
                newEntry[self.fieldNameMap[field]] = self.resolveFKeys(field, data[field])
            entries.append(newEntry)
        else:
            for ii in range(length):
                newEntry = {}
                for field in self.pkFields:
                    newEntry[self.fieldNameMap[field]] = self.resolveFKeys(field, data[field][ii])
                for field in self.otherFields:
                    newEntry[self.fieldNameMap[field]] =  self.resolveFKeys(field, data[field][ii])
                entries.append(newEntry)
        return entries

    def queryExistingEntry(self, entry: dict, timestamp: datetime.datetime) -> QuerySet:
        """
        Returns a QuerySet that searches for the given entry in the database.
        The returned QuerySet may not return an entry, but it will definitely never return more than one entry (unless the database has become corrupted)
        """
        pkArgs = {}
        for pk in self.pkFields:
            pkArgs[self.fieldNameMap[pk]] = entry[self.fieldNameMap[pk]]
        pkArgs[f"{self.startInternalField}__lte"] = timestamp
        pkArgs[f"{self.endInternalField}__gte"] = timestamp
        existing = self.model.objects.filter(**pkArgs)
        return existing

    def createEntry(self, entry: dict, timestamp: datetime.datetime):
        """
        Adds a new entry to the table, with a start time set to the given timestamp
        """
        newDict = entry.copy()
        newDict[self.startInternalField] = timestamp
        newModel = self.model(**newDict)
        newModel.save()

    def deleteEntry(self, entry: QuerySet, timestamp):
        """
        'Removes' the given entry from the database, by setting its end time to the given timestamp (which should be a time close to the time this function is called)
        """
        arg = {self.endInternalField: timestamp}
        entry.update(**arg)


class TemporalGetAPIView(TemporalFilterParams, GetAPIView):
    """
    Class-based View with a GET method for a temporal table.
    The class is empty because it just uses the normal GET method handling, with added support for the temporal filter params
    """
    pass


class TemporalPostAPIView(TemporalHelperMethods, View):
    """
    Class-based View with a POST method for a temporal table.
    """

    def post(self, request: HttpRequest, fmt=None, **kwargs):
        """
        Handler for POST requests.

        It 'removes' any existing entries, then creates new ones
        """
        timestamp = datetime.datetime.now(datetime.timezone.utc)
        data = self.getEntries(request)
        for entry in data:
            qSet = self.queryExistingEntry(entry, timestamp)
            if qSet.exists():
                self.deleteEntry(qSet, timestamp)
            self.createEntry(entry, timestamp)
        res = HttpResponse(status=204)
        return res


class TemporalPatchAPIView(TemporalHelperMethods, View):
    """
    Class-based View with a PATCH method for a temporal table.
    """

    def shouldReplaceEntry(self, oldQs: QuerySet, newEntry: dict) -> bool:
        """
        Used to determine whether a PATCH request should create a new entry to replace an existing entry.
        By default, will only create a new entry if none exist.

        Subclasses can override this method to change this behaviour.
        """
        if oldQs.exists():
            return False
        else:
            return True

    def patch(self, request: HttpRequest, fmt=None, **kwargs):
        """
        Handler for PATCH requests.

        Works like the POST handler, except it won't delete an existing entry unless it has changed.
        """
        timestamp = datetime.datetime.now(datetime.timezone.utc)
        data = self.getEntries(request)
        for entry in data:
            qSet = self.queryExistingEntry(entry, timestamp)
            if self.shouldReplaceEntry(qSet, entry):
                if qSet.exists():
                    self.deleteEntry(qSet, timestamp)
                self.createEntry(entry, timestamp)
        res = HttpResponse(status=204)
        return res


class TemporalDeleteAPIView(TemporalHelperMethods, View):
    """
    Class-based View with a DELETE method for a temporal table.
    """


    def validateDataDelete(self, data: dict) -> int:
        """
        Used instead of the method in the TemporalHelperMethods Class.
        This version only looks for primary key fields in the response data
        """
        if self.pkFields[0] in data:
            firstPk = data[self.pkFields[0]]
            if isinstance(firstPk, list):
                length = len(firstPk)
            else:
                length = 1
            for pk in self.pkFields:
                if pk in data:
                    pkVal = data[pk]
                    if isinstance(pkVal, list):
                        thisLen = len(pkVal)
                    else:
                        thisLen = 1
                    if thisLen != length:
                        raise InconsistentTemporalBody(
                            f"Field {pk} has length {thisLen}, expected {length}")
                else:
                    raise InconsistentTemporalBody(
                        f"Didn't find field {pk} in the request body")
            return length
        else:
            raise InconsistentTemporalBody(
                f"Didn't find field {self.pkFields[0]} in the request body")

    def getEntriesDelete(self, request: HttpRequest) -> list:
        """
        Gets the entries to be added to the table from the request data
        """
        data = handleRequestBody(request)
        entries = []
        length = self.validateDataDelete(data)
        if length == 1:
            newEntry = {}
            for field in self.pkFields:
                newEntry[self.fieldNameMap[field]] = data[field]
            entries.append(newEntry)
        else:
            for ii in range(length):
                newEntry = {}
                for field in self.pkFields:
                    newEntry[self.fieldNameMap[field]] = data[field][ii]
                entries.append(newEntry)
        return entries

    def delete(self, request: HttpRequest, fmt=None, **kwargs):
        """
        Handler for DELETE requests.
        """
        timestamp = datetime.datetime.now(datetime.timezone.utc)
        data = self.getEntriesDelete(request)
        for entry in data:
            qSet = self.queryExistingEntry(entry, timestamp)
            if qSet.exists():
                self.deleteEntry(qSet, timestamp)
        res = HttpResponse(status=204)
        return res

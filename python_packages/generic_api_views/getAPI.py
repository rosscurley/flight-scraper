"""
Classes to add a View with a generic GET request handler
"""
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.views import View

from .common import CheckParam, HasFieldNameMap, fieldInternalTypeMap
from .getFilters import FilterParams


class SliceIndexInvalidException(Exception):
    pass


class UnsupportedContentType(Exception):
    pass


class NoRequestBody(Exception):
    pass


class FieldFilterParams(HasFieldNameMap, CheckParam):
    """
    Generic class for limiting the fields returned by a request to the api
    """

    def isParamValid(self, param: str) -> bool:
        if param == "fields":
            return True
        else:
            return super().isParamValid(param)

    def getFields(self, request: HttpRequest) -> list:
        """
        Subclasses should call this method, passing the request.
        The returned array should be passed to the call to value()
        """
        fieldList = request.GET.getlist("fields")
        out = []
        for name in fieldList:
            if name in self.fieldNameMap:
                out.append(self.fieldNameMap[name])
        return out


class LimitOffsetParams(CheckParam):
    """
    Class that adds support for `limit`, `offset` and `page` query parameters.
    """

    def isParamValid(self, param):
        if param == "limit" or param == "offset" or param == "page":
            return True
        else:
            return super().isParamValid(param)

    def convertToInt(self, string: str) -> int:
        """
        Method for converting strings to ints, throwing a specific class of error if it fails
        """
        try:
            iVal = int(string)
        except ValueError:
            raise SliceIndexInvalidException(
                f"Could not convert value to an integer: {string}")
        return iVal

    def getSlice(self, request: HttpRequest) -> (int, int):
        """
        Checks for and parses the `limit`, `offset` and `page` query parameters, returning the start and end positions used to index the final query
        """
        qParams = request.GET
        limit = qParams.get("limit")
        offset = qParams.get("offset")
        page = qParams.get("page")
        if limit is not None:
            numResults = self.convertToInt(limit)
            if page is not None:
                iPage = self.convertToInt(page)
                if iPage < 0:
                    raise SliceIndexInvalidException("Page cannot be negative")
                startPos = iPage*numResults
                endPos = startPos + numResults
            elif offset is not None:
                startPos = self.convertToInt(offset)
                endPos = startPos+numResults
            else:
                startPos = 0
                endPos = numResults
        else:
            if offset is not None:
                startPos = self.convertToInt(offset)
                endPos = -1
            elif page is not None:
                raise SliceIndexInvalidException(
                    f"Cannot use page parameter without limit")
            else:
                startPos = 0
                endPos = -1

        if endPos < -1 or startPos <= -1:
            raise SliceIndexInvalidException(
                f"Invalid range calculated: {startPos}-{endPos}")
        else:
            if endPos != -1 and endPos < startPos:
                raise SliceIndexInvalidException(
                    f"Invalid range calculated: {startPos}-{endPos}")
            else:
                return startPos, endPos

    def shouldUseSlice(self, start: str, end: str) -> bool:
        """
        Indicates whether the start and end values should be used
        """
        if start == 0 and end == -1:
            return False
        else:
            return True


class OrderParams(HasFieldNameMap, CheckParam):
    """
    Class that adds support for the `order` query parameter
    """

    def isParamValid(self, param: str) -> bool:
        if param == "order":
            return True
        else:
            return super().isParamValid(param)

    def getOrderParams(self, request: HttpRequest) -> list:
        """
        Checks for and parses the `order` query parameter, returning the list of field names that can be passed to the order method on the QuerySet
        """
        orderList = request.GET.getlist("order")
        out = []
        for name in orderList:
            thisName: str = name
            reverse = False
            if thisName.startswith("-"):
                reverse = True
                thisName = thisName[1:]
            if thisName in self.fieldNameMap:
                outName = self.fieldNameMap[thisName]
                if reverse:
                    outName = "-" + outName
                out.append(outName)
        return out

class UnknownFormatException(Exception):
    pass

class GetAPIView(FilterParams, FieldFilterParams, LimitOffsetParams, OrderParams, View):
    """
    Class that defines a handler for GET requests
    """

    def getData(self, request: HttpRequest):
        """
        Method that retrieves the queried data from the database
        """
        filterArgs = self.getFilter(request)
        orderParams = self.getOrderParams(request)
        items = self.model.objects.all().filter(**filterArgs).order_by(*orderParams)
        start, end = self.getSlice(request)
        if self.shouldUseSlice(start, end):
            items = items[start:end]
        returnedFields = self.getFields(request)
        # TODO: make it handle foreign keys better
        return items.values(*returnedFields)

    def get(self, request: HttpRequest, fmt=None, **kwargs):
        """
        Handler for GET requests.
        """
        if fmt is None or fmt == "json":
            items = self.getData(request)
            return JsonResponse([item for item in items.iterator()], safe=False)
        else:
            # this should only ever really happen if something isn't configured properly in the urlconf, the user should never see this.
            raise UnknownFormatException(f"Unable to handle format {fmt}")


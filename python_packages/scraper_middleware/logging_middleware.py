""" Middleware for error logging
Author:         Ross Curley
Student id:     19098081
Last Edited:    3/10/2019
Last Edited by: Ross Curley
"""

import psycopg2
import sys

from configparser import ConfigParser
from .middleware import Middleware
from .exceptions import LoggingError

"""Logging_Middleware Extends Middleware
used to insert error and logging information into the database
"""
class Logging_Middleware(Middleware):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def insert_setting(self, key, valid_from, value, valid_to):
        """ Insert new setting into scraper settings table """
        sql =   """
                INSERT INTO scrapersettings(key, valid_from, value, valid_to) 
                VALUES(%s, %s, %s, %s)
                ON CONFLICT (key, valid_from)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (key, valid_from, value, valid_to))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise LoggingError("Error Inserting Scraper Setting :", err).with_traceback(tb)

    def insert_setting_list(self, setting_list):
        """ Insert new setting list into scraper settings table """
        sql =   """
                INSERT INTO scrapersettings(key, valid_from, value, valid_to) 
                VALUES(%s, %s, %s, %s)
                ON CONFLICT (key, valid_from)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, setting_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise LoggingError("Error Inserting Scraper Setting :", err).with_traceback(tb)

    def insert_session(self, target_site, session_start, session_end, errors, scraper_ip):
        """ Insert new session into scraper session table """
        sql =   """
                INSERT INTO scrapersessions(target_site, session_start, session_end, errors, scraper_ip) 
                VALUES(%s, %s, %s, %s, %s)
                ON CONFLICT (target_site, session_start)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (target_site, session_start, session_end, errors, scraper_ip))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise LoggingError("Error Inserting Scraper Session :", err).with_traceback(tb)

    def insert_session_list(self, session_list):
        """ Insert new session list into scraper session table """
        sql =   """
                INSERT INTO scrapersessions(target_site, session_start, session_end, errors, scraper_ip) 
                VALUES(%s, %s, %s, %s, %s)
                ON CONFLICT (target_site, session_start)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, session_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise LoggingError("Error Inserting Scraper Session :", err).with_traceback(tb)

    def insert_log(self, timestamp, target_url, target_site, session_start, flight_id, data, message, status):
        """ Insert new log into scraper log table """
        sql =   """
                INSERT INTO scraperlogs(timestamp, target_url, session_id, flight_id, data, message, status) 
                VALUES(%s, %s,
                (SELECT id FROM scrapersessions WHERE (target_site = %s) AND (session_start = %s)),
                (SELECT flight_number FROM flightinfo WHERE flight_number = %s), 
                %s, %s, %s)
                ON CONFLICT (timestamp, target_url)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (timestamp, target_url, target_site, session_start, flight_id, data, message, status))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise LoggingError("Error Inserting Scraper Log :", err).with_traceback(tb)
        
    def insert_log_list(self, session_list):
        """ Insert new log list into scraper log table """
        sql =   """
                INSERT INTO scraperlogs(timestamp, target_url, session_id, flight_id, data, message, status) 
                VALUES(%s, %s,
                (SELECT id FROM scrapersessions WHERE (target_site = %s) AND (session_start = %s)),
                (SELECT flight_number FROM flightinfo WHERE flight_number = %s), 
                %s, %s, %s)
                ON CONFLICT (timestamp, target_url)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, session_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise LoggingError("Error Inserting Scraper Log :", err).with_traceback(tb)

    def insert_error(self, id, log_timestamp, log_target_url, severity, error_details):
        """ Insert new error into scraper error table """
        sql =   """
                INSERT INTO scrapererrors(id, log_id, severity, error_details) 
                VALUES(%s, 
                (SELECT id FROM scraperlogs WHERE (timestamp = %s) AND (target_url = %s)),
                %s, %s)
                ON CONFLICT ON CONSTRAINT scrapererrors_pkey
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (id, log_timestamp, log_target_url, severity, error_details))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise LoggingError("Error Inserting Scraper Error :", err).with_traceback(tb)

    def insert_error_list(self, error_list):
        """ Insert new error into scraper error table """
        sql =   """
                INSERT INTO scrapererrors(id, log_id, severity, error_details) 
                VALUES(%s, 
                (SELECT id FROM scraperlogs WHERE (timestamp = %s) AND (target_url = %s)),
                %s, %s)
                ON CONFLICT ON CONSTRAINT scrapererrors_pkey
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, error_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise LoggingError("Error Inserting Scraper Error :", err).with_traceback(tb)

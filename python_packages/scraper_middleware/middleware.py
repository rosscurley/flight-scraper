""" Middleware for Flightscraper
Author:         Ross Curley
Student id:     19098081
Last Edited:    3/10/2019
Last Edited by: Ross Curley
"""
import psycopg2
import sys

from .exceptions import MiddlewareError
from .database_config import DATABASES

class Middleware:
    """ Initialise database connection """
    def __init__(self):
        try:
            self._params = self.config()
            self._db = self.connect(self._params)
        except:
            raise
    
    def config(self):
        
        db = {
            "database": DATABASES["default"]["NAME"],
            "user": DATABASES["default"]["USER"],
            "password": DATABASES["default"]["PASSWORD"],
            "host": DATABASES["default"]["HOST"],
            "port": DATABASES["default"]["PORT"],
        }
        

        return db

    
    def connect(self, params):
        """ Connect to PostgreSQL database server. """
        conn = None
        try:
            print('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)

            # create a cursor
            cur = conn.cursor()

            # execute a statement
            print('PostgreSQL database version:')
            cur.execute('SELECT version()')

            #display the postgreSQL database server version
            db_version = cur.fetchone()
            print(db_version)
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise MiddlewareError("Error Connecting to Database :").with_traceback(tb)
        
        return conn

    def close_connection(self):
        """ Close database connection. """
        try:
            self._db.close()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise MiddlewareError(err).with_traceback(tb)

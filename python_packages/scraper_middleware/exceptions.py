import datetime

class MiddlewareError(Exception):
    def __init__(self, message):
        self.message = message

class LoggingError(MiddlewareError):
    def __init__(self, message, errors):
        super().__init__(message)
        self.errors = errors
        self.timestamp = datetime.datetime.utcnow()

    def __str__(self):
        return "Logging Error:{0}\nOther Errors:{1}\n{2}".format(self.message, self.errors, self.timestamp)

class FlightdataError(MiddlewareError):
    def __init__(self, message, errors, sql):
        super().__init__(message)
        self.errors = errors
        self.sql = sql

    def __str__(self):
        return "Logging Error:{0}\nOther Errors:{1}\nReference SQL\n{2}".format(self.message, self.errors, self.sql)
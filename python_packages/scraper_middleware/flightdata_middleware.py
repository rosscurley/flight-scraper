"""Middleware for filghtdata
Author:         Ross Curley
Student id:     19098081
Last Edited:    3/10/2019
Last Edited by: Ross Curley
"""
import psycopg2
import sys
from .middleware import Middleware
from .exceptions import FlightdataError
from configparser import ConfigParser

"""Flightdata_Middleware Extends Middleware
used to insert error and logging information into the database
"""
class Flightdata_Middleware(Middleware):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)        

    def execute_sql(self, sql):
        """
        WARNING!!! 
        This method allows execution of raw SQL to be used for testing purposes only!!!
        """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Executing SQL :", err, sql).with_traceback(tb)

    def insert_airport(self, iata_code, latitude, longitude, name, timezone):
        """ Insert a new airport into the airports table. """
        sql =   """
                INSERT INTO airport(iata_code, latitude, longitude, name, timezone) 
                VALUES(%s, %s, %s, %s, %s)
                ON CONFLICT ON CONSTRAINT airport_pkey
                DO NOTHING
                RETURNING iata_code;
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (iata_code, latitude, longitude, name, timezone))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airport :", err, sql).with_traceback(tb)

    def insert_airport_list(self, airport_list):
        """ Insert multiple airports into airports table. """
        sql =   """
                INSERT INTO airport(iata_code, latitude, longitude, name, timezone) 
                VALUES(%s, %s, %s, %s, %s)
                ON CONFLICT ON CONSTRAINT airport_pkey
                DO NOTHING
                RETURNING iata_code;
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, airport_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airport :", err, sql).with_traceback(tb)

    def insert_aircraft(self, make, model, num_seats):
        """ Insert a new aircraft into the aircraft table. """
        sql =   """
                INSERT INTO aircraft(make, model, num_seats) 
                VALUES(%s, %s, %s)
                ON CONFLICT (make, model)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (make, model, num_seats))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Aircraft :", err, sql).with_traceback(tb)

    def insert_aircraft_list(self, aircraft_list):
        """ Insert multiple aircrafts into aircraft table. """
        sql =   """
                INSERT INTO aircraft(make, model, num_seats) 
                VALUES(%s, %s, %s)
                ON CONFLICT (make, model)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, aircraft_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Aircraft :", err, sql).with_traceback(tb)

    def insert_airline(self, name, website):
        """ Insert a new airline into the airline table. """
        sql =   """
                INSERT INTO airline(name, website) 
                VALUES(%s, %s)
                ON CONFLICT ON CONSTRAINT airline_pkey
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (name, website))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airline :", err, sql).with_traceback(tb)

    def insert_airline_list(self, airline_list):
        """ Insert a new airline list into the airline table. """
        sql =   """
                INSERT INTO airline(name, website) 
                VALUES(%s, %s)
                ON CONFLICT ON CONSTRAINT airline_pkey
                DO NOTHING
                """
        try:
             # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, airline_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airline :", err, sql).with_traceback(tb)

    def insert_flightinfo(self, flight_number, departure_time, arrival_time, origin, destination, airline, make, model):
        """ Insert a new flightinfo into the flightinfo database. """
        sql =   """
                INSERT INTO flightinfo(flight_number, departure_time, arrival_time, origin_id, destination_id, airline_id, aircraft_model_id)
                VALUES(
                    %s, %s, %s,
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    (SELECT name FROM airline WHERE name = %s),
                    (SELECT id FROM aircraft WHERE (make = %s) AND (model = %s))
                )
                ON CONFLICT ON constraint flightinfo_pkey
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (flight_number, departure_time, arrival_time, origin, destination, airline, make, model))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting FlightInfo :", err, sql).with_traceback(tb)

    def insert_flightinfo_list(self, flightinfo_list):
        """ Insert a new flightinfo list into the flightinfo database. """
        sql =   """
                INSERT INTO flightinfo(flight_number, departure_time, arrival_time, origin_id, destination_id, airline_id, aircraft_model_id)
                VALUES(
                    %s, %s, %s,
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    (SELECT name FROM airline WHERE name = %s),
                    (SELECT id FROM aircraft WHERE (make = %s) AND (model = %s))
                )
                ON CONFLICT ON constraint flightinfo_pkey
                DO NOTHING
                """
        try:
             # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, flightinfo_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting FlightInfo :", err, sql).with_traceback(tb)

    def insert_flightstatus(self, flight_id, time_collected, departure_time, departure_status, arrival_time, arrival_status):
        """ Insert a new flightstatus into the flightstatus database. """
        sql =   """
                INSERT INTO flightstatus(flight_id, time_collected, departure_time, departure_status, arrival_time, arrival_status)
                VALUES(
                    (SELECT flight_number FROM flightinfo WHERE flight_number = %s),
                    %s, %s, %s, %s, %s
                )
                ON CONFLICT (flight_id, time_collected)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (flight_id, time_collected, departure_time, departure_status, arrival_time, arrival_status))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting FlightStatus :", err, sql).with_traceback(tb)

    def insert_flightstatus_list(self, flightstatus_list):
        """ Insert a new flightstatus into the flightstatus database. """
        sql =   """
                INSERT INTO flightstatus(flight_id, time_collected, departure_time, departure_status, arrival_time, arrival_status)
                VALUES(
                    (SELECT flight_number FROM flightinfo WHERE flight_number = %s),
                    %s, %s, %s, %s, %s
                )
                ON CONFLICT (flight_id, time_collected)
                DO NOTHING
                """
        try:
             # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, flightstatus_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting FlightStatus :", err, sql).with_traceback(tb)

    def insert_airfare(self, flight, time_collected, fare_class, fare_type, fare):
        """ Insert a new airfare into the airfare table. """
        sql =   """
                INSERT INTO airfare(flight_id, time_collected, fare_class, fare_type, fare) 
                VALUES(
                    (SELECT flight_number FROM flightinfo WHERE flight_number = %s), %s, %s, %s, %s
                )
                ON CONFLICT (flight_id, time_collected, fare_class)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (flight, time_collected, fare_class, fare_type, fare))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airfare :", err, sql).with_traceback(tb)

    def insert_airfare_list(self, airfare_list):
        """ Insert a new airfare into the airfare table. """
        sql =   """
                INSERT INTO airfare(flight_id, time_collected, fare_class, fare_type, fare) 
                VALUES(
                    (SELECT flight_number FROM flightinfo WHERE flight_number = %s), %s, %s, %s, %s
                )
                ON CONFLICT (flight_id, time_collected, fare_class)
                DO NOTHING
                """
        try:
             # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, airfare_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting Airfare :", err, sql).with_traceback(tb)

    def insert_trackedroute(self, origin_id, destination_id, start_tracking, stop_tracking):
        """ Insert a new trackedroute into the trackedroutes table. """
        sql =   """
                INSERT INTO trackedroutes(origin_id, destination_id, start_tracking, stop_tracking) 
                VALUES(
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    %s, %s
                )
                ON CONFLICT (origin_id, destination_id, start_tracking)
                DO NOTHING
                """
        try:
            # Create new cursor
            cur = self._db.cursor()
            cur.execute(sql, (origin_id, destination_id, start_tracking, stop_tracking))
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting TrackedRoutes :", err, sql).with_traceback(tb)

    def insert_trackedroute_list(self, trackedroute_list):
        """ Insert a new trackedroute into the trackedroutes table. """
        sql =   """
                INSERT INTO trackedroutes(origin_id, destination_id, start_tracking, stop_tracking) 
                VALUES(
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    (SELECT iata_code FROM airport WHERE iata_code = %s),
                    %s, %s
                )
                ON CONFLICT (origin_id, destination_id, start_tracking)
                DO NOTHING
                """
        try:
             # Create new cursor
            cur = self._db.cursor()
            # execute INSERT statement
            cur.executemany(sql, trackedroute_list)
            # commit the changes with the database
            self._db.commit()
        except (Exception, psycopg2.DatabaseError) as err:
            if self._db is not None:
                self._db.close
            tb = sys.exc_info()[2]
            raise FlightdataError("Error Inserting TrackedRoutes", err, sql).with_traceback(tb)

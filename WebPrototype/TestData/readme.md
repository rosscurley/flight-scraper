# Test Data
This folder contains test data for the scraper and the web server.

## Other files
`other-test-data.json` contains the airports and airlines used with the test data in the folders

## Folders
Each folder corresponds to a website that we will be scraping, and will contain test data taken from that website.

## Test data format
For each set of test data, there will be three files:
- A `.html` file that contains a webpage
- A `.json` file that contains JSON
- A `.ini` file that contains metadata about the test data

### File names
The format of the file names for the test data will be 
```
TBD
```

### .html files
The `.html` files will contain a raw webpage, saved manually, that the scrapers should be able to parse and extract flight/airfare data from.

### .json files
The `.json` files will contain the data contained in the corresponding `.html` file.

For the scrapers, these files are what we expect the scraper to return after parsing the `.html` file.  
For the web server, these files serve as input data for the tests.

Format of these is still TBD.

### .ini files
The `.ini` files will contain metadata about the test data.  

These file will be formatted such that Python's `configparser` library can parse them:
- lines will be of the form `key=value`
- Sections can be made using `[section name]`
- All `key=value` entries must be in some section
- Comments can be added by starting a line with either `;` or `#`

The sections and keys included in the file may include:
- `[route]` info about the route
    - `source` The origin of the flights
    - `sourceIATA` IATA code of the source
    - `destination` The destination of the flights
    - `destinationIATA` IATA code of the destination
- `[request]` any relevant information that was submitted when requesting this data eg. through forms
    - Will probably depend on the website
    - `startTime` Start of the time range of the results
    - `endTime` End of the time range of results
    - `date` Date that results were requested for
- `[retrieval]` info about the retrieval of the test data
    - `time` Time when the data was retrieved
    - `browser` Browser used to download the data
    - `url` URL where the data was retrieved from , ie. the page that was saved
        - only if relevant, ie. if it contains a query string or could otherwise be used to retrieve the data again
- `[other]` any other miscellaneous information that may be relevant

## Usage
### Scrapers
When testing the scrapers, the `.html` files should be read and passed to the part of the scraper that parses html.
The JSON that is then returned should be compared to the contents of the `.json` file.
If the two match, the test should pass.

### Web server
When testing the web server, the `.json` files should be read in and sent to the web server in a HTTP request (probably a POST request).
Before sending the data, may need to add some extra fields (such as scraper run time) to simulate a real scraper run.  
Then the contents of the database should be checked to make sure the correct entries have been added

from django.test import TestCase
from .models import *
from django.utils import timezone
import datetime
import uuid


class AirlineTestCase(TestCase):
    def setUp(self):
        Airline.objects.create(
            name="Qantas", website="https://www.qantas.com/au/en.html")
        Airline.objects.create(
            name="Jetstar", website="https://www.jetstar.com/au/en/home")

    def test_str(self):
        qantas = Airline.objects.get(name="Qantas")
        self.assertEqual(
            qantas.__str__(), "name: Qantas\nwebsite: https://www.qantas.com/au/en.html")


class AirportTestCase(TestCase):
    def setUp(self):
        Airport.objects.create(
            iata_code="PER", 
            latitude=31.9385, 
            longitude=115.9672, 
            name="Perth Airport", 
            timezone="PER"
        )
        Airport.objects.create(
            iata_code="SYD", 
            latitude=33.9399, 
            longitude=151.1753,
            name="Sydney Airport",
            timezone="SYD"
        )

    def test_str(self):
        perth = Airport.objects.get(iata_code="PER")
        self.assertEqual(
            perth.__str__(), "IATA: PER\nlat: 31.9385\nlong: 115.9672\nPerth Airport\nPER"
        )


class AircraftTestCase(TestCase):
    def setUp(self):
        Aircraft.objects.create(make="Boeing", model="777", num_seats=396)
        Aircraft.objects.create(make="Airbus", model="a380-800", num_seats=868)
        Aircraft.objects.create(make="Boeing", model="747-400", num_seats=660)
        Aircraft.objects.create(make="Airbus", model="a320", num_seats=186)

    def test_str(self):
        aircraft = Aircraft.objects.get(make="Boeing", model="777")
        self.assertEqual(aircraft.__str__(),
                         "make: Boeing model: 777\nseats: 396")


class FlightInfoTestCase(TestCase):
    def setUp(self):
        self.dept_time = timezone.now() + datetime.timedelta(hours=2)
        self.arr_time = timezone.now() + datetime.timedelta(hours=8)
        ori = Airport.objects.create(
            iata_code="PER", latitude=31.9385, longitude=115.9672)
        dst = Airport.objects.create(
            iata_code="SYD", latitude=33.9399, longitude=151.1753)
        mdl = Aircraft.objects.create(
            make="Boeing", model="777", num_seats=396)
        airl = Airline.objects.create(
            name="Qantas", website="https://www.qantas.com/au/en.html")
        FlightInfo.objects.create(flight_number="BA101", departure_time=self.dept_time,
                                  arrival_time=self.arr_time, origin=ori,
                                  destination=dst, airline=airl, aircraft_model=mdl)

    def test_str(self):
        flight = FlightInfo.objects.get(flight_number="BA101")
        self.assertEqual(flight.__str__(), "flight: BA101\ndept: {} arr:{}\norg: PER dest: SYD\nairline: Qantas\naircraft: 777".format(
            self.dept_time, self.arr_time))


class FlightStatusTestCase(TestCase):
    def setUp(self):
        # setup flightinfo
        self.dept_time = timezone.now() + datetime.timedelta(hours=2)
        self.arr_time = timezone.now() + datetime.timedelta(hours=8)
        ori = Airport.objects.create(
            iata_code="PER", latitude=31.9385, longitude=115.9672)
        dst = Airport.objects.create(
            iata_code="SYD", latitude=33.9399, longitude=151.1753)
        mdl = Aircraft.objects.create(
            make="Boeing", model="777", num_seats=396)
        airl = Airline.objects.create(
            name="Qantas", website="https://www.qantas.com/au/en.html")
        flight = FlightInfo.objects.create(
            flight_number="BA101",
            departure_time=self.dept_time,
            arrival_time=self.arr_time,
            origin=ori,
            destination=dst,
            airline=airl,
            aircraft_model=mdl
        )
        self.time_collected = timezone.now()
        FlightStatus.objects.create(
            flight=flight,
            time_collected=self.time_collected,
            departure_time=self.dept_time,
            departure_status='SC',
            arrival_time=self.arr_time,
            arrival_status='SC'
        )

    def test_str(self):
        status = FlightStatus.objects.get(pk=1)
        self.assertEqual(status.__str__(), "flight: BA101 time collected: {}\ndept time: {} status: SC\narr time: {} arr status: SC".format(
            self.time_collected,
            self.dept_time,
            self.arr_time
        ))


class AirfareTestCase(TestCase):
    def setUp(self):
        dept_time = timezone.now() + datetime.timedelta(hours=2)
        arr_time = timezone.now() + datetime.timedelta(hours=8)
        ori = Airport.objects.create(
            iata_code="PER", latitude=31.9385, longitude=115.9672)
        dst = Airport.objects.create(
            iata_code="SYD", latitude=33.9399, longitude=151.1753)
        mdl = Aircraft.objects.create(
            make="Boeing", model="777", num_seats=396)
        airl = Airline.objects.create(
            name="Qantas", website="https://www.qantas.com/au/en.html")
        flight = FlightInfo.objects.create(
            flight_number="BA101",
            departure_time=dept_time,
            arrival_time=arr_time,
            origin=ori,
            destination=dst,
            airline=airl,
            aircraft_model=mdl
        )
        self.time_collected = timezone.now()
        Airfare.objects.create(flight=flight, time_collected=self.time_collected, fare_class='Economy', fare_type="OW", fare=300)

    def test_str(self):
        airfare = Airfare.objects.get(pk=1)
        self.assertEqual(airfare.__str__(), "flight: BA101 time: {} class: Economy\n type: OW\nfare: 300.00".format(self.time_collected))

"""
Django Models for FlightScraper Backend
see wiki for full database Schema
https://bitbucket.org/ComputingSEP/2019-06-flight-scraper/wiki/DatabaseSchema.md
Author:     Ross Curley
StudentID:  19098081
LastEdited: 28/5/19
Last Edited By: Ross Curley

A model is a class that inherits from models.Model. it represents a table
or collection in a Database. every attribute of the class is a field of the 
table or collection
"""
from django.db import models
from datetime import datetime, timezone as tz, timedelta
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
import uuid

# all foreign key on delete actions. will be changed to models.RESTRICT for finished product
FOREIGN_KEY_ON_DELETE = models.CASCADE

# latest possible time. We have to subtract a day to deal with timezones...
MAX_TIME = datetime.max.replace(
    tzinfo=timezone.get_current_timezone()) - timedelta(days=1)

# Airline
# Information for the airlines we're collecting data from


class Airline(models.Model):
    name = models.CharField(max_length=200, primary_key=True)
    website = models.URLField()

    def __str__(self):
        return "name: {}\nwebsite: {}".format(self.name, self.website)

    class Meta:
        db_table = "airline"

# Airport
# Start and end points of flights
class Airport(models.Model):
    iata_code = models.CharField("IATA code", max_length=3, primary_key=True)
    latitude = models.DecimalField(
        max_digits=7,
        decimal_places=4,
        default=0.0,
        validators=[MaxValueValidator(90.0), MinValueValidator(-90)]
    )
    longitude = models.DecimalField(
        max_digits=7,
        decimal_places=4,
        default=0.0,
        validators=[MaxValueValidator(180.0), MinValueValidator(-180.0)]
    )
    name = models.CharField(max_length=50)
    TIMEZONE_CHOICE =(
        ('ADE', 'Australia/Adelaide'),
        ('BRI', 'Australia/Brisbane'),
        ('BRH', 'Australia/Broken_Hill'),
        ('CUR', 'Australia/Currie'),
        ('DAR', 'Australia/Darwin'),
        ('EUC', 'Australia/Eucla'),
        ('HOB', 'Australia/Hobart'),
        ('LIN', 'Australia/Lindeman'),
        ('LOH', 'Australia/Lord_Howe'),
        ('MEL', 'Australia/Melbourne'),
        ('PER', 'Australia/Perth'),
        ('SYD', 'Australia/Sydney'),
        ('UTC', 'Etc/Utc')
    )
    timezone = models.CharField(max_length=3, choices=TIMEZONE_CHOICE, default='UTC')

    def __str__(self):
        return "IATA: {0}\nlat: {1}\nlong: {2}\n{3}\n{4}".format(self.iata_code, self.latitude, self.longitude, self.name, self.timezone)

    class Meta:
        db_table = "airport"

# Aircraft
# Information about planes


class Aircraft(models.Model):
    make = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    num_seats = models.PositiveSmallIntegerField()

    def __str__(self):
        return "make: {} model: {}\nseats: {}".format(self.make, self.model, self.num_seats)

    class Meta:
        db_table = "aircraft"
        unique_together = (('make', 'model'),)
        verbose_name_plural = 'aircraft'

# FlightInfo
# basic information about a flight which will be assumed to not change
# departure_time and arrival_time will be the values initially retrieved for the
# flight; the flightstatus table will show any changes to these values after
# initial entry


class FlightInfo(models.Model):
    flight_number = models.CharField(max_length=7, primary_key=True)
    departure_time = models.DateTimeField('Departure time')
    arrival_time = models.DateTimeField('Arrival time')
    origin = models.ForeignKey(
        Airport, on_delete=FOREIGN_KEY_ON_DELETE, related_name='flight_origin')
    destination = models.ForeignKey(
        Airport, on_delete=FOREIGN_KEY_ON_DELETE, related_name='flight_destination')
    airline = models.ForeignKey(Airline, on_delete=FOREIGN_KEY_ON_DELETE)
    aircraft_model = models.ForeignKey(
        Aircraft, on_delete=FOREIGN_KEY_ON_DELETE)

    def __str__(self):
        return "flight: {}\ndept: {} arr:{}\norg: {} dest: {}\nairline: {}\naircraft: {}".format(
            self.flight_number,
            self.departure_time,
            self.arrival_time,
            self.origin.iata_code,
            self.destination.iata_code,
            self.airline.name,
            self.aircraft_model.model
        )

    class Meta:
        db_table = "flightinfo"
        verbose_name_plural = 'flight info'

# FlightStatus
# Information collected about a flight during its tracking period.
# Both departureStatus and arrivalStatus can have the values:
# scheduled: The flight is still scheduled to happen in the future,
#     the times are scheduled times
# unknown: flight data could not be found for some reason,
#     the times will be NULL
# cancelled: the flight has been cancelled, the times will
#     be NULL
# estimated: the plane has taken off or landed, and the time is an estimate
#
# departureStatus can also have the values:
#     departed: the plane has taken off, and departureTime is accurate
#
# arrivalStatus can also have the values:
#     departed: then plane has taken off, but not yet landed.
#         arrivalTime is an estimate
#     landed: the plane has landed, and arrivalTime is accurate


class FlightStatus(models.Model):
    flight = models.ForeignKey(FlightInfo, on_delete=FOREIGN_KEY_ON_DELETE)
    time_collected = models.DateTimeField('Time Collected')
    departure_time = models.DateTimeField('Departure time')
    # DEPT_STATUS_CHOICES = (
    #     ('SC', 'scheduled'),
    #     ('UN', 'unknown'),
    #     ('CA', 'cancelled'),
    #     ('DE', 'departed')
    # )
    departure_status = models.CharField(
        max_length=20,
        # choices=DEPT_STATUS_CHOICES
    )
    arrival_time = models.DateTimeField('Arrival time')
    # ARR_STATUS_CHOICES = (
    #     ('SC', 'scheduled'),
    #     ('UN', 'unknown'),
    #     ('CA', 'cancelled'),
    #     ('DE', 'departed'),
    #     ('LA', 'landed')
    # )
    arrival_status = models.CharField(
        max_length=20,
        # choices=ARR_STATUS_CHOICES
    )

    def __str__(self):
        return "flight: {} time collected: {}\ndept time: {} status: {}\narr time: {} arr status: {}".format(
            self.flight.flight_number,
            self.time_collected,
            self.departure_time,
            self.departure_status,
            self.arrival_time,
            self.arrival_status
        )

    class Meta:
        db_table = "flightstatus"
        unique_together = (('flight', 'time_collected'),)
        verbose_name_plural = 'flight statuses'


# Airfare
# Airfare data collected for a flight during its tracking period.
# fareClass indicates the class of ticket the price is for,
# eg: business, economy etc.
# fareType indicates whether the fare is for a one-way trip or a round trip
# fare is the cost of the ticket in Australian Dollars
class Airfare(models.Model):
    flight = models.ForeignKey(FlightInfo, on_delete=FOREIGN_KEY_ON_DELETE)
    time_collected = models.DateTimeField('Time Collected')
    fare_class = models.CharField(max_length=20)
    fare_type = models.CharField(max_length=20)
    fare = models.DecimalField(
        "airfare",
        max_digits=8,
        decimal_places=2,
        validators=[MinValueValidator(0.0)]
    )

    def __str__(self):
        return "flight: {} time: {} class: {}\n type: {}\nfare: {}".format(
            self.flight.flight_number,
            self.time_collected,
            self.fare_class,
            self.fare_type,
            self.fare
        )

    class Meta:
        db_table = "airfare"
        unique_together = (('flight', 'time_collected', 'fare_class'),)

# TrackedRoute
# Stores the routes to be tracked, so it doesn't need to be hard-coded into the scraper.
# startTracking and stopTracking show the range of dates that the route
# was/is being tracked for. This is used to ensure that a record of when
# each route was being tracked is kept. When a route is added, the current
# time is used for startTracking, and for stopTracking, the latest possible
# time that can be stored is used. To "remove" a route, its current entry
# (the one with startTracking <= now <= stopTracking) should have
# stopTracking set to the current time. Below is an example of how the
# routes that were being tracked at a particular point in time could be
# queried.


class TrackedRoute(models.Model):
    origin = models.ForeignKey(
        Airport, on_delete=FOREIGN_KEY_ON_DELETE, related_name='route_origin')
    destination = models.ForeignKey(
        Airport, on_delete=FOREIGN_KEY_ON_DELETE, related_name='route_destination')
    start_tracking = models.DateTimeField('Tracking period start')
    stop_tracking = models.DateTimeField(
        'Tracking period end', default=MAX_TIME)

    def __str__(self):
        return "{} - {}".format(self.origin, self.destination)

    def is_active(self):
        return self.start_tracking <= timezone.now() and self.stop_tracking > timezone.now()
    is_active.admin_order_field = "start_tracking"
    is_active.boolean = True
    is_active.short_description = "Currently tracked?"

    def clean(self):
        if self.stop_tracking < self.start_tracking:
            raise ValidationError(
                {'stop_tracking': '"Stop tracking" must be after "Start tracking"'})

    class Meta:
        db_table = "trackedroutes"
        unique_together = (('origin', 'destination', 'start_tracking'),)
        verbose_name = "tracked route"
        constraints = [
            models.CheckConstraint(
                check=models.Q(stop_tracking__gt=models.F('start_tracking')),
                name='start_before_stop'
            )
        ]

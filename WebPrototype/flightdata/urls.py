from django.urls import path, include

from . import views, api_views

api_patterns = [
    path('airports', api_views.AirportAPIView.as_view(), name='api_airports'),
    path('airlines', api_views.AirlineAPIView.as_view(), name='api_airlines'),
    path('aircraft', api_views.AircraftAPIView.as_view(), name="api_aircraft"),
    path('flights', api_views.FlightInfoAPIView.as_view(), name='api_flight_info'),
    path('flightstatus', api_views.FlightStatusAPIView.as_view(),
         name='api_flight_status'),
    path('airfares', api_views.AirfareAPIView.as_view(), name="api_airfares"),
    path('routes', api_views.TrackedRouteAPIView.as_view(), name="api_routes"),
]

app_name = "flightdata"
urlpatterns = [
    # ex: /flightdata/airports
    path('airports', views.AirportView.as_view(), name='airports'),
    # ex: /flightdata/airlines
    path('airlines', views.AirlineView.as_view(), name='airlines'),
    # ex: /flightdata/aircraft
    path('aircraft', views.AircraftView.as_view(), name='aircrafts'),
    # ex: /flightdata/flights
    path('flights', views.FlightInfoView.as_view(), name='flight_info'),
    # ex: /flightdata/flights/<flight_number>
    path('flights/<str:flight_number>',
         views.FlightStatusView.as_view(), name='flight_status'),
    # ex: /flightdata/airfares
    path('airfares', views.AirfareView.as_view(), name='airfares'),
    # ex: /flightdata/airfares/<flight_number>
    path('airfares/<str:flight_number>', views.AirfareFlightView.as_view(), name='airfare'),
    # ex: /flightdata/routes
    path('routes', views.TrackedRouteView.as_view(), name='routes'),
    path('api/', include((api_patterns, "json")), {"fmt":"json"}),
    path('csv/', include((api_patterns, "csv")), {"fmt":"csv"}),
]

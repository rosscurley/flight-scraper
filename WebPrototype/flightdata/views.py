"""
Django views for FlightData backend
Author: Jack Lardner, Ross Curley
Date last modified: 29/10/19

Views process a user's request and returns a response
"""

from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from django.views import View
from django.views.generic import ListView, DetailView
from decimal import Decimal
import datetime
from .models import *


class AirportView(View):
    """View for the Airport model"""

    template = 'flightdata/airports.html'

    def get(self, request):
        airport_list = Airport.objects.all()
        context = {
            'airport_list': airport_list,
        }
        return render(request, self.template, context)


class AirlineView(View):
    """View for the Airline model"""

    template = 'flightdata/airlines.html'

    def get(self, request):
        airline_list = Airline.objects.all()
        context = {
            'airline_list': airline_list,
        }
        return render(request, self.template, context)


class AircraftView(View):
    """View for the Aircraft model"""

    template = 'flightdata/aircraft.html'

    def get(self, request):
        aircraft_list = Aircraft.objects.all()
        context = {
            'aircraft_list': aircraft_list,
        }
        return render(request, self.template, context)

def is_valid_query(param):
        return param != '' and param is not None

class FlightInfoView(View):
    """View for the FlightInfo model"""

    template = 'flightdata/flights.html'

    def filter(self, request):
        """Filter request from html form and return query results."""
        # start with every results
        query = FlightInfo.objects.all().order_by('-departure_time') 
        # get fields from get request
        flight = request.GET.get('search_flight')
        airline = request.GET.get('airline')
        origin = request.GET.get('origin_airport')
        destination = request.GET.get('destination_airport')
        depart = request.GET.get('departure_date')
        arrive = request.GET.get('arrival_date')
        results = request.GET.get('results')

        # for every valid query filter dater accordingly 
        try:
            if (is_valid_query(flight)):
                query = query.filter(flight_number__icontains=flight)

            if (is_valid_query(airline)):
                query = query.filter(airline__exact=airline)

            if (is_valid_query(origin)):
                query = query.filter(origin__iata_code__iexact=origin)

            if (is_valid_query(destination)):
                query = query.filter(destination__iata_code__iexact=destination)

            if (is_valid_query(depart)):
                query = query.filter(departure_time__gte=depart)
            
            if (is_valid_query(arrive)):
                query = query.filter(arrival_time__lte=arrive)

            if (is_valid_query(results)):
                query = query[:int(results)]
        except (ValidationError, Exception) as err:
            print(err)
        finally:
            return query

    def get(self, request):
        """ handle get request from user """
        # filter request from user
        flight_info_list = self.filter(request)
        airline_list = Airline.objects.all()
        airports = Airport.objects.all()
        context = {
            'flights': flight_info_list,
            'airlines': airline_list,
            'airports': airports
        }
        return render(request, self.template, context)

class FlightStatusView(View):
    """View for the FlightStatus model"""

    template = 'flightdata/flightstatus.html'

    def get(self, request, flight_number):
        """ filter get request from user."""
        flight_status_list = FlightStatus.objects.filter(
            flight_id__exact=flight_number)
        context = {
            'flights': flight_status_list,
            'flight_number': flight_number
        }
        return render(request, self.template, context)


class AirfareView(View):
    """View for the Airfare model"""

    template = 'flightdata/airfares.html'

    def filter(self, request):
        """Filter request from html form and return query results."""
        # start with every results
        query = Airfare.objects.all()
        # get fields from get request
        search_class = request.GET.get("search_class" )
        search_type = request.GET.get("search_type")
        min_fare = request.GET.get("min_fare")
        max_fare = request.GET.get("max_fare")
        flight = request.GET.get("search_flight")
        date = request.GET.get("search_date")
        results = request.GET.get("results")

        # for every valid query filter dater accordingly 
        try:
            if is_valid_query(search_class):
                query = query.filter(fare_class__icontains=search_class)
            
            if is_valid_query(search_type):
                query = query.filter(fare_type__icontains=search_type)
            
            if is_valid_query(min_fare) and is_valid_query(max_fare):
                query = query.filter(fare__range=(float(min_fare), float(max_fare)))
            elif is_valid_query(min_fare):
                query = query.filter(fare__gte=float(min_fare))
            elif is_valid_query(max_fare):
                query = query.filter(fare__lte=float(max_fare))

            if is_valid_query(flight):
                query = query.filter(flight__flight_number__icontains=flight)

            if is_valid_query(date):
                query = query.filter(time_collected__gte=date)

            if (is_valid_query(results)):
                query = query[:int(results)]
        except (ValidationError, Exception) as err:
            print(err)
        finally:
            return query
           

    def get(self, request):
        """ handle get request from user."""
        airfares = self.filter(request)
        context = {
            'airfares': airfares,
        }
        return render(request, self.template, context)

class AirfareFlightView(View):
    """" View for a single flights airfare. """

    template = 'flightdata/flight_airfare.html'

    def get(self, request, flight_number):
        """ handle get request from users."""
        airfare_list = Airfare.objects.filter(flight_id__exact=flight_number)
        context = {
            'airfare_list': airfare_list,
            'flight_number': flight_number
        }
        return render(request, self.template, context)


class TrackedRouteView(View):
    """View for the TrackedRoutes model"""

    template = 'flightdata/routes.html'

    def filter(self, request):
        """Filter request from html form and return query results."""
        # start with every results
        query = TrackedRoute.objects.all().order_by('-start_tracking')
        # get fields from get request
        origin = request.GET.get("origin")
        destination = request.GET.get("dest")
        start = request.GET.get("start")
        stop = request.GET.get("stop")
        results = request.GET.get("results")

        try:
            if is_valid_query(origin):
                query = query.filter(origin__iata_code__exact=origin)

            if is_valid_query(destination):
                query = query.filter(destination__iata_code__exact=destination)

            if is_valid_query(stop):
                query = query.filter(stop_tracking__lte=stop)
                
            if is_valid_query(start):
                query = query.filter(start_tracking__gte=start)
            
            if is_valid_query(results):
                query = query[:int(results)] 
        except (ValidationError, OverflowError, Exception) as err:
            print(err)
        finally:
            return query

    def get(self, request):
        """filter get requests from users"""
        routes = self.filter(request)
        airports = Airport.objects.all()
        context = {
            'routes': routes,
            'airports': airports
        }
        return render(request, self.template, context)



from django.http import HttpResponse, HttpRequest, JsonResponse
from django.views import View
from django.db.models import Model

from generic_api_views.getAPI import GetAPIView
from generic_api_views.temporalAPI import TemporalDeleteAPIView, TemporalGetAPIView, TemporalPatchAPIView, TemporalPostAPIView
from generic_api_views.postAPI import PostAPIView
from generic_api_views.csvAPI import CSVGetAPIView

from . import models


class AirportAPIView(CSVGetAPIView, PostAPIView):
    fieldNameMap = {
        "IATACode": "iata_code",
        "longitude": "longitude",
        "latitude": "latitude",
        "name": "name",
        "timezone": "timezone",
    }

    model = models.Airport


class AirlineAPIView(CSVGetAPIView, PostAPIView):
    fieldNameMap = {
        "name": "name",
        "website": "website",
    }
    model = models.Airline


class AircraftAPIView(CSVGetAPIView, PostAPIView):
    fieldNameMap = {
        "make": "make",
        "model": "model",
        "numSeats": "num_seats",
    }
    model = models.Aircraft


class FlightInfoAPIView(CSVGetAPIView):
    fieldNameMap = {
        "flightNumber": "flight_number",
        "departureTime": "departure_time",
        "arrivalTime": "arrival_time",
        "origin": "origin",
        "destination": "destination",
        "airline": "airline",
        "aircraftModel": "aircraft_model"
    }
    model = models.FlightInfo


class FlightStatusAPIView(CSVGetAPIView):
    fieldNameMap = {
        "flight": "flight",
        "timeCollected": "time_collected",
        "departureTime": "departure_time",
        "departureStatus": "departure_status",
        "arrivalTime": "arrival_time",
        "arrivalStatus": "arrival_status",
    }
    model = models.FlightStatus


class AirfareAPIView(CSVGetAPIView):
    fieldNameMap = {
        "flight": "flight",
        "timeCollected": "time_collected",
        "fareClass": "fare_class",
        "fareType": "fare_type",
        "fare": "fare",
    }
    model = models.Airfare


class TrackedRouteAPIView(CSVGetAPIView, TemporalGetAPIView, TemporalPostAPIView, TemporalPatchAPIView, TemporalDeleteAPIView):
    fieldNameMap = {
        "origin": "origin",
        "destination": "destination",
        "startTracking": "start_tracking",
        "stopTracking": "stop_tracking",
    }
    model = models.TrackedRoute
    startInternalField = "start_tracking"
    endInternalField = "stop_tracking"
    pkFields = [
        "origin",
        "destination",
    ]

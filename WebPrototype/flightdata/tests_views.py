"""
Test for the views located in views.py
Author: Jack Lardner
Date last modified: 25/05/19

Test classes are tagged with 'view' to easily run all view tests at once
without running other tests

Run these tests using 'python manage.py test flightdata --tag view
"""

from datetime import datetime, timezone, timedelta
from django.test import TestCase, Client, tag
from django.urls import reverse
from .models import FlightStatus, FlightInfo, Airport, Airline, Aircraft


@tag('view')
class AirportViewTest(TestCase):
    def test_get(self):
        client = Client()
        response = client.get(reverse('flightdata:airports'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'flightdata/airports.html')


@tag('view')
class AirlineViewTest(TestCase):
    def test_get(self):
        client = Client()
        response = client.get(reverse('flightdata:airlines'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('flightdata/airlines.html')


@tag('view')
class AircraftViewTest(TestCase):
    def test_get(self):
        client = Client()
        response = client.get(reverse('flightdata:aircrafts'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('flightdata/aircraft.html')


@tag('view')
class FlightInfoViewTest(TestCase):
    def test_get(self):
        client = Client()
        response = client.get(reverse('flightdata:flight_info'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('flightdata/flights.html')


@tag('view')
class FlightStatusViewTest(TestCase):
    def setUp(self):
        """Sets up the data models for the test"""

        self.aircraft = Aircraft.objects.create(
            make='Boeing',
            model='747',
            num_seats=150
        )

        self.qantas = Airline.objects.create(
            name='Qantas',
            website='www.qantas.com.au'
        )

        self.destination = Airport.objects.create(
            iata_code='ADE',
            latitude=27.149,
            longitude=81.767,
            name='Adelaide Airport'
        )

        self.origin = Airport.objects.create(
            iata_code='PER',
            latitude=34.513,
            longitude=73.157,
            name='Perth Airport'
        )

        self.flightinfo = FlightInfo.objects.create(
            flight_number='AR321',
            departure_time=datetime(
                2019, 5, 25, 15, 15, 00, tzinfo=timezone(timedelta(hours=8))
            ),
            arrival_time=datetime(
                2019, 5, 26, 4, 00, 00, tzinfo=timezone(timedelta(hours=8))
            ),
            origin=self.origin,
            destination=self.destination,
            airline=self.qantas,
            aircraft_model=self.aircraft
        )

        self.flightstatus = FlightStatus.objects.create(
            flight=self.flightinfo,
            time_collected=datetime(
                2019, 5, 15, 18, 00, 00, tzinfo=timezone(timedelta(hours=8))
            ),
            departure_time=datetime(
                2019, 5, 25, 15, 15, 00, tzinfo=timezone(timedelta(hours=8))
            ),
            departure_status='SC',
            arrival_time=datetime(
                2019, 5, 26, 4, 00, 00, tzinfo=timezone(timedelta(hours=8))
            ),
            arrival_status='SC'
        )

    def test_get(self):
        client = Client()
        response = client.get(
            reverse('flightdata:flight_status', args=['AR321']))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('flightdata/flightstatus.html')


@tag('view')
class AirfareViewTest(TestCase):
    def test_get(self):
        client = Client()
        response = client.get(reverse('flightdata:airfares'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('flightdata/airfares.html')


@tag('view')
class TrackedRoutesViewTest(TestCase):
    def test_get(self):
        client = Client()
        response = client.get(reverse('flightdata:routes'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('flightdata/routes.html')

from django.contrib import admin
from .models import *
from django.utils import timezone


class AirlineAdmin(admin.ModelAdmin):
    list_filter = []
    search_fields = ['name', 'website']


admin.site.register(Airline, AirlineAdmin)


class AirportAdmin(admin.ModelAdmin):
    list_display = ("iata_code", "name")
    list_filter = []
    search_fields = ['iata_code', 'latitude', 'longitude']


admin.site.register(Airport, AirportAdmin)


class AircraftAdmin(admin.ModelAdmin):
    list_display = ("make", "model")
    list_filter = ['num_seats']
    search_fields = ['make', 'model']


admin.site.register(Aircraft, AircraftAdmin)


class FlightInfoAdmin(admin.ModelAdmin):
    list_display = ("flight_number", "origin", "destination", "airline")
    list_filter = ['departure_time', 'arrival_time']
    search_fields = ['flight_number', 'origin__iata_code',
                     'destination__iata_code', 'airline__name']


admin.site.register(FlightInfo, FlightInfoAdmin)


class FlightStatusAdmin(admin.ModelAdmin):
    list_display = ("flight", "time_collected")
    list_filter = ['time_collected', "departure_status",
                   'departure_time', 'arrival_status', 'arrival_time']
    search_fields = ['flight_number']


admin.site.register(FlightStatus, FlightStatusAdmin)


class AirfareAdmin(admin.ModelAdmin):
    list_display = ("flight", "time_collected")
    list_filter = ['time_collected', 'fare_class', 'fare_type']
    search_fields = ['flight']


admin.site.register(Airfare, AirfareAdmin)


class CurrentRouteFilter(admin.SimpleListFilter):
    title = "Tracked routes"
    parameter_name = "tracking"

    def lookups(self, request, model_admin):
        return (
            ('active', 'Currently tracked routes'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'active':
            now = timezone.now()
            return queryset.filter(start_tracking__lte=now, stop_tracking__gte=now)


class TrackedRouteAdmin(admin.ModelAdmin):
    list_display = ("origin", "destination", "start_tracking",
                    "stop_tracking", "is_active")
    list_filter = ['start_tracking', CurrentRouteFilter]
    search_fields = ['origin__iata_code', 'destination__iata_code']


admin.site.register(TrackedRoute, TrackedRouteAdmin)

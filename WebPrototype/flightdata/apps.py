from django.apps import AppConfig


class FlightdataConfig(AppConfig):
    name = 'flightdata'

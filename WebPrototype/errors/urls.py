from django.urls import path, include

from . import views, api_views

api_patterns = [
    path('scrapersettings', api_views.ScraperSettingAPIView.as_view(),
         name='api_scraper_settings'),
    # ex: /errors/scrapersessions
    path('scrapersessions', api_views.ScraperSessionAPIView.as_view(),
         name='api_scraper_sessions'),
    # ex: /errors/scraperlogs
    path('scraperlogs', api_views.ScraperLogAPIView.as_view(),
         name='api_scraper_logs'),
    # ex: /errors/scrapererrors
    path('scrapererrors', api_views.ScraperErrorAPIView.as_view(),
         name='api_scraper_errors'),
]

app_name = "errors"
urlpatterns = [
    # ex: /errors/scrapersettings
    path('scrapersettings', views.ScraperSettingView.as_view(),
         name='scraper_settings'),
    # ex: /errors/scrapersessions
    path('scrapersessions', views.ScraperSessionView.as_view(),
         name='scraper_sessions'),
    # ex: /errors/scraperlogs
    path('scraperlogs', views.ScraperLogView.as_view(), name='scraper_logs'),
    # ex: /errors/scrapererrors
    path('scrapererrors', views.ScraperErrorView.as_view(), name='scraper_errors'),
    path('api/', include((api_patterns, "json")), {"fmt":"json"}),
    path('csv/', include((api_patterns, "csv")), {"fmt":"csv"}),
]

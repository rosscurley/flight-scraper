from datetime import datetime, timezone, timedelta
from django.test import TestCase, Client, tag
from django.urls import reverse
from django.apps import apps
from .models import *


@tag('view')
class ScraperSettingsViewTest(TestCase):
    def setUp(self):
        self.settings = ScraperSetting.objects.create(
            key='Test',
            valid_from=timezone.now() - timedelta(days=7),
            value='Test',
            valid_to=timezone.now() + timedelta(days=7)
        )

    def test_get(self):
        client = Client()
        response = client.get(reverse('errors:scraper_settings'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('errors/scrapersettings.html')


@tag('view')
class ScraperSessionsViewTest(TestCase):
    def test_get(self):
        client = Client()
        response = client.get(reverse('errors:scraper_sessions'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('errors/scrapersessions.html')


@tag('view')
class ScraperLogsViewTest(TestCase):
    def test_get(self):
        client = Client()
        response = client.get(reverse('errors:scraper_logs'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('errors/scraperlogs.html')


@tag('view')
class ScraperErrorsViewTest(TestCase):
    def test_get(self):
        client = Client()
        response = client.get(reverse('errors:scraper_errors'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('errors/scrapererrors.html')

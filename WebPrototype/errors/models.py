from django.db import models
from datetime import datetime, timezone as tz, timedelta
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from django.apps import apps
import uuid

# all foreign key on delete actions. will be changed to models.RESTRICT for finished product
FOREIGN_KEY_ON_DELETE = models.CASCADE

# latest possible time. We have to subtract a day to deal with timezones...
MAX_TIME = datetime.max.replace(
    tzinfo=timezone.get_current_timezone()) - timedelta(days=1)

# TODO: change these comment blocks to docstrings in the classes
# ScraperSetting
# Stores the scrapers settings, such as the tracking period to be used.
#
# The settings are stored as key-value pairs. This allows us the flexibility
# to add new settings without having to update the database schema each time.
# It also means that when a settings value is changed, only that settings
# value needs to be re-entered. If a setting is absent at some point in time,
# then it should be assumed that there is some default value that the
# scsraper will use.
#
# validFrom and validTo serve the same purpose as startTracking and
# stopTracking from the TrackedRoutes table, and should be used in the same
# way when adding or "removing" an entry. To change a settings value,
# validTo on the old entry should be changed to the current time, and a new
# entry should be added with the current time and the new value (so validTo
# on the old entry and validFrom on the new entry should have the same value).


class ScraperSetting(models.Model):
    key = models.CharField(max_length=200)
    valid_from = models.DateTimeField('Setting valid from')
    value = models.CharField(max_length=200)
    valid_to = models.DateTimeField('Setting valid to', default=MAX_TIME)

    def __str__(self):
        return "{} {} {} {}".format(self.key, self.valid_from, self.value, self.valid_to)

    def is_active(self):
        return self.valid_from <= timezone.now() and self.valid_to > timezone.now()
    is_active.admin_order_field = "valid_from"
    is_active.boolean = True
    is_active.short_description = "Currently set?"

    def clean(self):
        if self.valid_to < self.valid_from:
            raise ValidationError(
                {'valid_to': '"Valid to" must be after "Valid from"'})

    class Meta:
        db_table = "scrapersettings"
        unique_together = (('key', 'valid_from'),)
        verbose_name = 'scraper setting'
        constraints = [
            models.CheckConstraint(
                check=models.Q(valid_to__gt=models.F('valid_from')),
                name='from_before_to'
            )
        ]

# ScraperSession
# Records each run of the scraper. Will have an entry each time a website is
# scraped, which may involve getting data for a number of flights.
#
# errors indicates whether any errors occurred during this scraping session.
# It can have the values: fatal: an error caused the scraper to stop early
# while scraping the site recoverable: errors occurred while getting data
# for some flights, but the scraper was able to continue. Attempts were made to retrieve all required data warning: All required data was able
# to be retrieved, but some issues may have occurred none: All required data
# was retrieved without any issues
#
# The ScraperLogs table shows when, and for which particular URLs, any
# errors occurred, and the ScraperErrors table will give the full details of
# the errors.


class ScraperSession(models.Model):
    target_site = models.URLField()
    session_start = models.DateTimeField('Session Started')
    session_end = models.DateTimeField('Session Ended')
    SCRAPER_ERRORS_CHOICES = (
        ("f", "fatal error"),
        ("r", "recoverable error"),
        ("w", "warning"),
        ("n", "none")
    )
    errors = models.CharField(max_length=1, choices=SCRAPER_ERRORS_CHOICES)
    scraper_ip = models.GenericIPAddressField()

    def __str__(self):
        return "target :{}\nstart:{} end:{}\nerrors:\n{}\nip :{}".format(
            self.target_site,
            self.session_start,
            self.session_end,
            self.errors,
            self.scraper_ip
        )

    class Meta:
        db_table = "scrapersessions"
        unique_together = (('target_site', 'session_start'),)
        verbose_name = 'scraper session'

# ScraperLog
# Has information about each webpage scraped.
#
# targetURL should be the full URL that was scraped, and timestamp should be
# the exact time the HTML was received from the webpage, preferably from the
# response header.
#
# data indicates what type of data was being scraped. It can be flightStatus
# or airfare.
#
# message can be a short message that can be returned by the scraper to give
# some information about how the scraping went. It can be NULL.
#
# status indicates how well the scraping went. It can be: success: Data was
# successfully retrieved. warning: Data was successfully retrieved, but minor
# issues may have occurred error: Errors occurred while scraping, but the
# scraper was able to recover and retrieve the data * eg: The first HTTP
# request may have returned an error code, but after retrying the request it
# was successful failed: An error occurred while scraping, and no data could
# be retrieved


class ScraperLog(models.Model):
    timestamp = models.DateTimeField('Timestamp')
    target_url = models.URLField()
    session = models.ForeignKey(
        ScraperSession, on_delete=FOREIGN_KEY_ON_DELETE)
    flight = models.ForeignKey(
        'flightdata.FlightInfo', on_delete=FOREIGN_KEY_ON_DELETE)
    DATA_CHOICES = (
        ("F", "flight status"),
        ("A", "airfare")
    )
    data = models.CharField(max_length=1, choices=DATA_CHOICES)
    message = models.CharField(max_length=200, blank=True)
    STATUS_CHOICES = (
        ('S', 'Success'),
        ('W', 'Warning'),
        ('E', 'Error'),
        ("F", "Failed")
    )
    status = models.CharField(max_length=1, choices=STATUS_CHOICES)

    def __str__(self):
        return "timestamp: {} target:{}\nsession: {} {}\nflight: {} data: {}\nmessage:\n{}\nstatus: {}".format(
            self.timestamp,
            self.target_url,
            self.session.target_site,
            self.session.session_start,
            self.flight.flight_number,
            self.data,
            self.message,
            self.status
        )

    class Meta:
        db_table = "scraperlogs"
        unique_together = (('timestamp', 'target_url'),)
        verbose_name = 'scraper log'

# ScraperError
# Gives detailed information about any errors or warnings that occur while
# scraping.
#
# timestamp and targetURL identify which attempt at scraping caused the issue.
#
# ID will be an integer used as the tables primary key, in case more than one
# error occurs while scraping a single URL.
#
# severity shows the severity of the error. It can have the values:
#     fatal: the error caused the whole scraping run to end early
#     error: the error caused the scraper to be unable to retrieve data from
#         this particular URL
#     recoverable: the scraper was able to recover from the error and
#         retrieve the data warning: an issue occurred, but did not directly
#         interfere with the running of the scraper
#
# errorDetails will be any relevant information about the error, such as
# error messages, stack traces, or HTTP status codes.


class ScraperError(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    log = models.ForeignKey(ScraperLog, on_delete=FOREIGN_KEY_ON_DELETE)
    SEVERITY_CHOICES = (
        ('F', 'Fatal'),
        ('E', 'Error'),
        ('R', 'Recoverable'),
        ('W', 'Warning')
    )
    severity = models.CharField(
        max_length=1,
        choices=SEVERITY_CHOICES
    )
    error_details = models.CharField(max_length=200)

    def __str__(self):
        return "id: {}\n{} {}\nseverity: {}\ndetails:\n{}".format(
            self.id,
            self.log.timestamp,
            self.log.target_url,
            self.severity,
            self.error_details
        )

    class Meta:
        db_table = "scrapererrors"
        verbose_name = 'scraper error'

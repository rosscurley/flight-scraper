from django.test import TestCase
from .models import *
from django.apps import apps
from django.utils import timezone
import datetime
import uuid


class ScraperSettingTestCase(TestCase):
    def setUp(self):
        key = "key"
        self.valid_from = timezone.now() - datetime.timedelta(days=60)
        value = 1
        self.valid_to = timezone.now()
        ScraperSetting.objects.create(
            key=key, valid_from=self.valid_from, value=value, valid_to=self.valid_to)

    def test_str(self):
        setting = ScraperSetting.objects.get(pk=1)
        self.assertEqual(setting.__str__(), "key {} 1 {}".format(
            self.valid_from, self.valid_to))


class ScraperSessionTestCase(TestCase):
    def setUp(self):
        site = "https://www.qantas.com/au/en.html"
        self.start = timezone.now() - datetime.timedelta(days=2)
        self.end = timezone.now()
        errors = "n"
        ip = '127.0.0.1'
        ScraperSession.objects.create(
            target_site=site, session_start=self.start, session_end=self.end, errors=errors, scraper_ip=ip)

    def test_str(self):
        session = ScraperSession.objects.get(
            target_site="https://www.qantas.com/au/en.html", session_start=self.start)
        self.assertEqual(
            session.__str__(),
            "target :https://www.qantas.com/au/en.html\nstart:{} end:{}\nerrors:\nn\nip :127.0.0.1".format(
                self.start,
                self.end
            )
        )


class ScraperLogTestCase(TestCase):
    def setUp(self):
        self.timestamp = timezone.now()
        target = "https://www.qantas.com/au/en.html"
        self.start = timezone.now() - datetime.timedelta(days=2)
        self.end = timezone.now()
        errors = "n"
        ip = "127.0.0.1"
        self.session = ScraperSession.objects.create(
            target_site=target, session_start=self.start, session_end=self.end, errors=errors, scraper_ip=ip)
        dept_time = timezone.now() + datetime.timedelta(hours=2)
        arr_time = timezone.now() + datetime.timedelta(hours=8)
        ori = apps.get_model('flightdata', 'Airport').objects.create(
            iata_code="PER", latitude=31.9385, longitude=115.9672)
        dst = apps.get_model('flightdata', 'Airport').objects.create(
            iata_code="SYD", latitude=33.9399, longitude=151.1753)
        mdl = apps.get_model('flightdata', 'Aircraft').objects.create(
            make="Boeing", model="777", num_seats=396)
        airl = apps.get_model('flightdata', 'Airline').objects.create(
            name="Qantas", website="https://www.qantas.com/au/en.html")
        flight = apps.get_model('flightdata', 'FlightInfo').objects.create(
            flight_number="BA101",
            departure_time=dept_time,
            arrival_time=arr_time,
            origin=ori,
            destination=dst,
            airline=airl,
            aircraft_model=mdl
        )
        data = 'S'
        msg = "Minim magna pariatur culpa velit nulla voluptate pariatur non reprehenderit quis sit culpa ex est."
        status = 'S'
        ScraperLog.objects.create(timestamp=self.timestamp, target_url=target,
                                  session=self.session, flight=flight, data=data, message=msg, status=status)

    def test_str(self):
        log = ScraperLog.objects.get(
            timestamp=self.timestamp, target_url="https://www.qantas.com/au/en.html")
        self.assertEqual(
            log.__str__(),
            "timestamp: {} target:https://www.qantas.com/au/en.html\nsession: https://www.qantas.com/au/en.html {}\nflight: BA101 data: S\nmessage:\nMinim magna pariatur culpa velit nulla voluptate pariatur non reprehenderit quis sit culpa ex est.\nstatus: S"
            .format(
                self.timestamp,
                self.session.session_start,
            )
        )


class ScraperErrorTestCase(TestCase):
    def setUp(self):
        self.id = uuid.uuid4()
        timestamp = timezone.now()
        target = "https://www.qantas.com/au/en.html"
        start = timezone.now() - datetime.timedelta(days=2)
        end = timezone.now()
        errors = "n"
        ip = "127.0.0.1"
        session = ScraperSession.objects.create(
            target_site=target, session_start=start, session_end=end, errors=errors, scraper_ip=ip)
        dept_time = timezone.now() + datetime.timedelta(hours=2)
        arr_time = timezone.now() + datetime.timedelta(hours=8)
        ori = apps.get_model('flightdata', 'Airport').objects.create(
            iata_code="PER", latitude=31.9385, longitude=115.9672)
        dst = apps.get_model('flightdata', 'Airport').objects.create(
            iata_code="SYD", latitude=33.9399, longitude=151.1753)
        mdl = apps.get_model('flightdata', 'Aircraft').objects.create(
            make="Boeing", model="777", num_seats=396)
        airl = apps.get_model('flightdata', 'Airline').objects.create(
            name="Qantas", website="https://www.qantas.com/au/en.html")
        flight = apps.get_model('flightdata', 'FlightInfo').objects.create(
            flight_number="BA101",
            departure_time=dept_time,
            arrival_time=arr_time,
            origin=ori,
            destination=dst,
            airline=airl,
            aircraft_model=mdl
        )
        data = 'S'
        msg = "Minim magna pariatur culpa velit nulla voluptate pariatur non reprehenderit quis sit culpa ex est."
        status = 'S'
        self.log = ScraperLog.objects.create(
            timestamp=timestamp, target_url=target, session=session, flight=flight, data=data, message=msg, status=status)
        severity = 'F'
        details = "Et ut eiusmod ad id et eu elit."
        ScraperError.objects.create(
            id=self.id, log=self.log, severity=severity, error_details=details)

    def test_str(self):
        error = ScraperError.objects.get(id=self.id)
        self.assertEqual(error.__str__(), "id: {}\n{} {}\nseverity: F\ndetails:\nEt ut eiusmod ad id et eu elit.".format(
            self.id, self.log.timestamp, self.log.target_url))

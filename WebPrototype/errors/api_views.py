

from django.http import HttpResponse, HttpRequest, JsonResponse
from django.views import View
from django.db.models import Model, QuerySet

from generic_api_views.getAPI import GetAPIView
from generic_api_views.common import handleRequestBody
from generic_api_views.temporalAPI import TemporalDeleteAPIView, TemporalGetAPIView, TemporalPatchAPIView, TemporalPostAPIView, InconsistentTemporalBody
from generic_api_views.csvAPI import CSVGetAPIView

from . import models


class ScraperSettingAPIView(TemporalPostAPIView, TemporalPatchAPIView, TemporalDeleteAPIView, CSVGetAPIView, TemporalGetAPIView):
    fieldNameMap = {
        "key": "key",
        "validFrom": "valid_from",
        "value": "value",
        "validTo": "valid_to",
    }
    model = models.ScraperSetting
    startInternalField = "valid_from"
    endInternalField = "valid_to"
    pkFields = ["key"]
    otherFields = ["value"]

    def getEntries(self, request: HttpRequest) -> list:
        """
        This endpoint will accept a series of key-value pairs to add/change/delete, 
        instead of a list of values for each field.
        """
        data = handleRequestBody(request)
        entries = []
        for key in data:
            if isinstance(data[key], (list, dict)):
                raise InconsistentTemporalBody(f"{key} is not a scalar value")
            entries.append({"key": key, "value": f"{data[key]}"})
        return entries
    
    def shouldReplaceEntry(self, oldQs: QuerySet, newEntry: dict) -> bool:
        """
        New entries should be created if there is an existing one, as long as it has a different value.
        """
        if oldQs.exists():
            oldInst = oldQs.get()
            if oldInst.value == newEntry["value"]:
                return False
            else:
                return True
        else:
            return True






class ScraperSessionAPIView(CSVGetAPIView):
    fieldNameMap = {
        "targetSite": "target_site",
        "sessionStart": "session_start",
        "sessionEnd": "session_end",
        "errors": "errors",
        "scraperIp": "scraper_ip",
    }
    model = models.ScraperSession


class ScraperLogAPIView(CSVGetAPIView):
    fieldNameMap = {
        "timestamp": "timestamp",
        "targetUrl": "target_url",
        "session": "session",
        "flight": "flight",
        "data": "data",
        "message": "message",
        "status": "status",
    }
    model = models.ScraperLog


class ScraperErrorAPIView(CSVGetAPIView):
    fieldNameMap = {
        "id": "id",
        "log": "log",
        "severity": "severity",
        "errorDetails": "error_details",
    }
    model = models.ScraperError

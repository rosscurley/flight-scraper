from django.contrib import admin
from .models import *
from django.utils import timezone


class CurrentSettingFilter(admin.SimpleListFilter):
    title = "Used values"
    parameter_name = "active"

    def lookups(self, request, model_admin):
        return (
            ('active', 'Currently active values'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'active':
            now = timezone.now()
            return queryset.filter(valid_from__lte=now, valid_to__gte=now)


class ScraperSettingAdmin(admin.ModelAdmin):
    fields = ["key", "value", "valid_from",  "valid_to"]
    list_display = ("key", "value", "valid_from",  "valid_to", "is_active")
    list_filter = ['valid_from', CurrentSettingFilter]
    search_fields = ['key', 'value']


admin.site.register(ScraperSetting, ScraperSettingAdmin)


class ScraperSessionAdmin(admin.ModelAdmin):
    list_display = ("target_site", "session_start", "errors")
    list_filter = ['errors', 'session_start']
    search_fields = ['target_site']


admin.site.register(ScraperSession, ScraperSessionAdmin)


class ScraperLogAdmin(admin.ModelAdmin):
    list_display = ("target_url", "timestamp", "data", "status")
    list_filter = ['data', 'status', 'timestamp']
    search_fields = ['target_url']


admin.site.register(ScraperLog, ScraperLogAdmin)


class ScraperErrorsAdmin(admin.ModelAdmin):
    list_display = ("id", "log", "severity")
    list_filter = ['severity', 'log__timestamp']
    search_fields = ['log__target_url']


admin.site.register(ScraperError, ScraperErrorsAdmin)

"""
Django views for Errors backend
Author: Jack Lardner, Ross Curley
Date last modified: 29/10/19

Views process a user's request and returns a response
"""

from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.views import View
from django.views.generic import ListView, DetailView

from .models import *


class ScraperSettingView(View):
    """View for the ScraperSetting model"""

    template = 'errors/scrapersettings.html'

    def filter(self, request):
        query = ScraperSetting.objects.all().order_by('-valid_from')
        key = request.GET.get("search_key")
        valid_from = request.GET.get("valid_from")
        valid_to = request.GET.get("valid_to")
        results = request.GET.get("results")

        try:
            if is_valid_query(key):
                query = query.filter(key__icontains=key)

            if is_valid_query(valid_from):
                query = query.filter(valid_from__gte=valid_from)

            if is_valid_query(valid_to):
                query = query.filter(valid_to__lte=valid_to)

            if (is_valid_query(results)):
                query = query[:int(results)]    
        except (ValidationError, Exception) as err:
            print(err)
        finally:
            return query

    def get(self, request):
        settings = self.filter(request)
        context = {
            'settings': settings
        }
        return render(request, self.template, context)


class ScraperSessionView(View):
    """View for the ScraperSession model"""

    template = 'errors/scrapersessions.html'

    def filter(self, request):
        """filter get requests from user"""
        query = ScraperSession.objects.all().order_by('-session_start')
        target = request.GET.get("target_site")
        start = request.GET.get("start")
        end = request.GET.get("end")
        errors = request.GET.get("errors")
        scraper = request.GET.get("scraper_ip")
        results = request.GET.get("results")

        try:
            if is_valid_query(target):
                query = query.filter(target_site__icontains=target)

            if is_valid_query(start):
                query = query.filter(session_start__gte=start)

            if is_valid_query(end):
                query = query.filter(session_end__lte=end)

            if is_valid_query(errors):
                query = query.filter(errors__exact=errors)

            if is_valid_query(scraper):
                query = query.filter(scraper_ip__icontains=scraper)

            if (is_valid_query(results)):
                query = query[:int(results)]    
        except (ValidationError, Exception) as err:
            print(err)
        finally:
            return query

    def get(self, request):
        sessions = self.filter(request)
        context = {
            'sessions': sessions
        }
        return render(request, self.template, context)


class ScraperLogView(ListView):
    """View for the ScraperLog model"""

    template = 'errors/scraperlogs.html'

    def filter(self, request):
        """filters get requests from users"""
        query = ScraperLog.objects.all().order_by('-timestamp')
        date = request.GET.get("date_search")
        url = request.GET.get("url_search")
        flight = request.GET.get("flight_search")
        type_search = request.GET.get("type_search")
        status = request.GET.get("status_search")
        results = request.GET.get("results")

        try:
            if (is_valid_query(date)):
                query = query.filter(timestamp__gte=date)

            if (is_valid_query(url)):
                query = query.filter(target_url__icontains=url)

            if is_valid_query(flight):
                query = query.filter(flight__flight_number__icontains=flight)

            if is_valid_query(type_search):
                query = query.filter(data__iexact=type_search)

            if is_valid_query(status):
                query = query.filter(status__iexact=status)

            if (is_valid_query(results)):
                query = query[:int(results)]    
        except (ValidationError, Exception) as err:
            print(err)
        finally:
            return query
        
    def get(self, request):
        scraperlogs = self.filter(request)
        context = {
            'scraperlogs': scraperlogs
        }
        return render(request, self.template, context)


class ScraperErrorView(ListView):
    """View for the ScraperError model"""

    template = 'errors/scrapererrors.html'

    def filter(self, request):
        """filter get requests from users"""
        query = ScraperError.objects.all().order_by('-log')
        error_id = request.GET.get("error_id")
        log_date = request.GET.get("log_date")
        severity = request.GET.get("severity")
        results = request.GET.get("results")

        try:
            if is_valid_query(error_id):
                query = query.filter(id__icontains=error_id)

            if is_valid_query(log_date):
                query = query.filter(log_timestamp__gte=log_date)

            if is_valid_query(severity):
                query = query.filter(severity__exact=severity)
            
            if (is_valid_query(results)):
                query = query[:int(results)]    
        except (ValidationError, Exception) as err:
            print(err)
        finally:
            return query

    def get(self, request):
        """handles get request from users"""
        errors = self.filter(request)
        context = {
            'errors': errors
        }
        return render(request, self.template, context)

def is_valid_query(param):
    """checks if a query is valid or not"""    
    return param != '' and param is not None
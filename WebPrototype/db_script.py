""" Script for loading test data from JSON file into Django models

Author:     Ross Curley
StudentID:  19098081
LastEdited: 30/8/19
Last Edited By: Ross Curley

to run the script execute with parameters
py -3 db_script.py [1|2] [filename]

[1]: load miscellaneous data such as airports and airlines
[2]: load flightinfo and flight status
[filename]: file must be in JSON format https://www.json.org/
"""
import os
import django
import sys
import json
from django.core.exceptions import FieldDoesNotExist, FieldError, ObjectDoesNotExist
from django.utils import timezone as tz
from datetime import datetime, timedelta, timezone

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'WebPrototype.settings')
django.setup()

from flightdata.models import Airline, Airport, Aircraft, FlightInfo, FlightStatus, Airfare


class Misc_Loader:
    """Takes a json file and opens it. Can then load data into flightdata
    models.

    Attributes:
        _data: a json file containing the test data
    """

    def __init__(self, filename):
        """Initiates the Misc_loader class taking a filename as params
        then opens the filename and assign the open file to _data
        """
        try:
            with open (filename, "r") as read_file:
                self._data = json.load(read_file)
        except json.JSONDecodeError as err:
            print("JSON decode error {0}.".format(err))
            raise AttributeError("Error in loader object creation")

    def load_airline(self):
        """With the _data create Airline objects"""
        try:
            for x in self._data['Airline']:
                # Only create a new Airline objects if one doesn't already exist
                if not Airline.objects.filter(name=x['name']).exists():
                    try:
                        new_airline = Airline.objects.create(
                            name=x['name'],
                            website=x['website']
                        )
                    except FieldDoesNotExist as err:
                        print("Field does not exist {0}".format(err))
                    except FieldError as err:
                        print("Field error {0}".format(err))
        except KeyError as err:
            print("Error in function load_airline {0}".format(err))    

    def load_airport(self):
        """With the _data create Airport objects"""
        try:
            for x in self._data['Airport']:
                # Only create a new Airport if one doesn't already exist
                if not Airport.objects.filter(iata_code=x['iata_code']).exists():
                    try:
                        Airport.objects.create(
                            iata_code=x['iata_code'],
                            latitude=x['latitude'],
                            longitude=x['longitude'],
                            name=x['name']
                        )
                    except FieldDoesNotExist as err:
                        print("Field does not exist {0}".format(err))
                    except FieldError as err:
                        print("Field error {0}".format(err))
        except KeyError as err:
            print("Error in function load_airport {0}".format(err))    

    def load_aircraft(self):
        """with the _data create Aircraft objects"""
        try:
            for x in self._data['Aircraft']:
                # Only create a new Aircraft if one doesn't already exist
                if not Aircraft.objects.filter(make=x['make'], model=x['model'],).exists():
                    try:
                        Aircraft.objects.create(
                            make=x['make'],
                            model=x['model'],
                            num_seats=x['num_seats']
                        )
                    except FieldDoesNotExist as err:
                        print("Field does not exist {0}".format(err))
                    except FieldError as err:
                        print("Field error {0}".format(err))
        except KeyError as err:
            print("Error in function load_aircraft {0}".format(err))    

class Flightdata_Loader:
    """Takes a json file and opens it. Can then load data into flightdata
    models.

    Attributes:
        _data: a json file containing the test data
    """
    def __init__(self, filename):
        """Initiates the Misc_loader class taking a filename as params
        then opens the filename and assign the open file to _data
        """
        with open (filename, "r") as read_file:
            self._data = json.load(read_file)

    def load_flightinfo(self):
        """with the _data create FligthInfo objects"""
        try:
            for x in self._data['FlightInfo']:
                # if an object doesn't exist 
                if not FlightInfo.objects.filter(flight_number=x['flight_number']).exists():
                    # retreive origin and dest for database
                    orig = Airport.objects.get(iata_code=x['origin'])
                    dest = Airport.objects.get(iata_code=x['destination'])
                    # retrieve airline from database
                    airl = Airline.objects.get(name=x['airline'])
                    airc = x['aircraft_model'].split()
                    # retrieve aircraft from database if it exists
                    if Aircraft.objects.filter(make=airc[0], model=airc[1]).exists():
                        craft = Aircraft.objects.get(make=airc[0], model=airc[1])
                    else:   # else create new one
                        craft = Aircraft.objects.create(
                            make=airc[0],
                            model=airc[1],
                            num_seats = 0 # number of seats will be unknown
                        )
                    # create object
                    try:
                        FlightInfo.objects.create(
                            flight_number=x['flight_number'],
                            departure_time=self.parse_time(x['departure_time']),
                            arrival_time=self.parse_time(x['arrival_time']),
                            origin=orig,
                            destination=dest,
                            airline=airl,
                            aircraft_model=craft,
                        )
                    except FieldDoesNotExist as err:
                        print("Field does not exist {0}".format(err))
                    except FieldError as err:
                        print("Field error {0}".format(err))            
        except KeyError as err:
            print("Error in function load_flightinfo {0}".format(err))    

    def load_flightstatus(self):
        """with the _data create FligthStatus objects"""
        try:
            for x in self._data['FlightStatus']:
                try:
                    # get flightinfo from database
                    flight = FlightInfo.objects.get(flight_number=x['flight'])
                    if not FlightStatus.objects.filter(flight=flight).exists():
                        # create object
                        FlightStatus.objects.create(
                            flight=flight,
                            time_collected=tz.now(),
                            departure_time=self.parse_time(x['departure_time']),
                            departure_status=x['departure_status'],
                            arrival_time=self.parse_time(x['arrival_time']),
                            arrival_status=x['arrival_status']
                        )
                    else:
                        # update status
                        try:
                            status = FlightStatus.objects.get(flight=flight)
                            status.time_collected=tz.now()
                            status.departure_time=self.parse_time(x['departure_time'])
                            status.departure_status=x['departure_status']
                            status.arrival_time=self.parse_time(x['arrival_time'])
                            status.arrival_status=x['arrival_status']
                            status.save()
                        except ObjectDoesNotExist as err:
                            print("Could Not find Status {0}".format(err))
                except FieldDoesNotExist as err:
                        print("Field does not exist {0}".format(err))
                except FieldError as err:
                        print("Field error {0}".format(err))

        except KeyError as err:
            print("Error in function load_flightstatus {0}".format(err))    

    def load_airfare(self):
        """with the _data create FligthStatus objects"""
        try:
            for x in self._data['Airfare']:
                # if airfare doesn't exist
                if not Airfare.objects.filter(flight=x['flight'], fare_class=x['fare_class'], fare_type=x['fare_type']).exists():
                    # get flightinfo from database 
                    flight = FlightInfo.objects.get(flight_number=x['flight'])
                    # cerate object
                    try:
                        Airfare.objects.create(
                            flight=flight,
                            time_collected=tz.now(),
                            fare_class=x['fare_class'],
                            fare_type=x['fare_type'],
                            fare=(x['fare'])
                        )
                    except FieldDoesNotExist as err:
                        print("Field does not exist {0}".format(err))
                    except FieldError as err:
                        print("Field error {0}".format(err))
        except KeyError as err:
            print("Error in function load_airfare {0}".format(err))            

    def parse_time(self, str):
        """takes a date string in the format 
        'HH:MM DD/MM/YYY' and creates and returns a datetime object
        """
        x = str.split()
        time = x[0].split(':')
        date = x[1].split('/')
        
        return datetime(int(date[2]), int(date[1]), int(date[0]), int(time[0]), int(time[1]), tzinfo=timezone(timedelta(hours=8)))

if __name__ == "__main__":
    if len(sys.argv) == 3:
        if int(sys.argv[1]) == 1:
            try:
                loader = Misc_Loader(sys.argv[2])
                loader.load_airport()
                loader.load_airline()
                loader.load_aircraft()
                fLoader = Flightdata_Loader(sys.argv[2])
                fLoader.load_flightinfo()
            except AttributeError as err:
                print(err)
        elif int(sys.argv[1]) == 2:
            try:
                loader = Flightdata_Loader(sys.argv[2])
                loader.load_flightinfo()
                loader.load_flightstatus()
                loader.load_airfare()
            except AttributeError as err:
                print(err)

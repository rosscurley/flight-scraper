from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.views import View
from django.views.generic import ListView, DetailView
from flightdata.models import FlightStatus
from errors.models import ScraperSetting
from django.utils import timezone
import uuid
import time


class IndexView(View):
    template = 'WebPrototype/index.html'

    def get(self, request):
        # get 5 most recent flight statuses
        flight_list = FlightStatus.objects.all().order_by('-time_collected')[:5]
        context = {
            'flight_list': flight_list
        }
        return render(request, self.template, context)

class SettingsView(View):
    template = 'WebPrototype/settings.html'

    def get(self, request):
        setting = ScraperSetting.objects.latest('valid_from')
        context = {
            'setting': setting
        }
        return render(request, self.template, context)

    def post(self, request):
        setting = self.process_post(request)
        context = {
            'setting': setting
        }
        return render(request, self.template, context)

    def process_post(self, request):
        metro = request.POST.get('metro')
        regional = request.POST.get('regional')
        setting = ScraperSetting.objects.latest('valid_from')
        new_value = {}
        new_value["metro"], new_value["regional"] = [],[]

        if is_valid_query(metro) and is_valid_query(regional):
            if is_valid_query(metro):
                m = metro.split(';')
                for i in m:
                    if is_valid_time(i):
                        new_value["metro"].append(i)
            if is_valid_query(regional):
                r = regional.split(';')
                for i in r:
                    if is_valid_time(i):
                        new_value["regional"].append(i)

            setting.valid_to = timezone.now()
            setting.save()
            ScraperSetting.objects.create(key=uuid.uuid4(), valid_from=timezone.now(), value=new_value)
        
        return ScraperSetting.objects.latest('valid_from')
        
def is_valid_query(param):
    return param != '' and param is not None

def is_valid_time(param):
    try:
        time.strptime(param, '%H:%M')
        return True
    except ValueError:
        return False
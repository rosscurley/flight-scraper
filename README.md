# Python Virtual Environment
1.  To setup your own python virtual environment type:  
    Linux:  
        `sudo apt-get install python3-venv    # If needed`  
        `python3 -m venv env`  

    Windows:  
        `python -m venv env`

2.  To activate the environment type:  
    Linux:
        `source env/bin/activate`

    Windows:
        `env/Scripts/Active`  

3.  Use pip to install required packages:  
        `pip install -r requirements.txt`  

4.  To deactivate the enironment type  
    Linux:
        `deactivate`

    Windows:    
        `env/Scripts/deactivate`  
    
NOTE: type "Set-ExecutionPolicy Unrestricted" in powershell if you are getting script execution restriction

## PYTHONPATH
If you're using the scrapers, add the `./packages` folder to your `$PYTHONPATH` environment variable

# Installation
1.  Setup postgresql database on own machine  
    see: http://www.postgresqltutorial.com/install-postgresql/ for installation instructions.

2.  In the `/WebPrototype/WebPrototype` directory, create a file called `usersettings.py` and add the following code, changing the indicated lines as needed

    ```python
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'flightscraper',  #change
            'USER': 'postgres_username',  #change
            'PASSWORD': 'postgres_password',  #change
            'HOST': '127.0.0.1',
            'PORT': '5432',
        }
    }
    ```
    usersettings.py is in the .gitignore file, so it won't be tracked by git.
    This way, we can each have different values for the settings.
    Otherwise we would have to either deal with a merge conflict here every time we do a merge, or we'd have to make sure we never commit the changes in this part, which could make it difficult to change anything else in the settings file.  
    We should probably figure out a better way to handle this in the future.

3.  In terminal type  
        `python3 manage.py makemigrations flightdata`  
        `python3 migrate flightdata`  
        `python3 sqlmigrate flightdata 0001`  
    

4.  To start the development server type  
        `python3 manage.py runserver`  
        
NOTE: do not modify or delete manage.py

# Splash
1.  Install Splash through Docker:

    ```
    sudo docker pull scrapinghub/splash:3.3.1
    ```


2.  Start Splash:

    ```
    sudo docker run -it -p 8050:8050 scrapinghub/splash:3.3.1 --disable-private-mode
    ```

    Port 8050 is used in this example, however any port can be used so long as it is updated in Scrapy's settings.py
    
    NOTE: ```--disable-private-mode``` flag must be used for some websites to render correctly

3.  Configure Scrapy
	
	Add the following to scrapy's settings.py:
	
	```python
	SPLASH_URL = 'http://0.0.0.0:8050/'
	DUPEFILTER_CLASS = 'scrapy_splash.SplashAwareDupeFilter'
	HTTPCACHE_STORAGE = 'scrapy_splash.SplashAwareFSCacheStorage'
	...
	# Add the following to SPIDER_MIDDLEWARES
	SPIDER_MIDDLEWARES = {
	   'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
	}
	...
	# Add the following to DOWNLOADER_MIDDLEWARES
	DOWNLOADER_MIDDLEWARES = {
	   'scrapy_splash.SplashCookiesMiddleware': 723,
	   'scrapy_splash.SplashMiddleware': 725,
	   'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
	}



# Setting up and using Jetstar/REX scrapers
1. With pip installed, run
    `pip install scrapy`

2. Download the Google Chrome web drivers here:
    https://chromedriver.chromium.org/downloads
    Make sure to download the correct driver for your installed version of Chrome

3. Place the driver in your C directory (path should look like this: C:\chromedriver.exe)

4. Make sure you're in the directory of the scraper itself (/scrapers/jetstar | /scrapers/rex)

5. Run the scraper using `scrapy crawl (jetstar | rex) -a days=DAYS -a dest=DEST`
    Optional: append `-o flights.json` to direct the output into a file named flights.json
    
# Linter
To run the linter on a specific file, run:
```
autopep8 -i <file name>
```

To run the linter on all files, run:
```
autopep8 -i -r ./
```
# Frontend
The frontend is in the `frontend/` folder.
See the readme there for more details.

TODO: set up the web server to serve the frontend files.

# Scrapers
Each scraper should be in a separate folder in the `./scrapers/` folder.

Any spiders should extend `LoggingSpider` from `general.logging_spider`, and use its methods to log information about each target scraped, and any errors that occurred.  
When the spider is finished (even if due to an error), it should call the `onSessionEnd` method of `LoggingSpider`.

# Middleware
to use the middleware users need to create a database ini file in this format
```ini
[postgresql]
host=localhost
database=flightdata
user=username
password=password
```
By default, the middleware will look for the file: `./python_packages/scraper_middleware/database.ini`, and will look under the section `postgresql`, but these can be set to other values using the `filename` and `section` keyword arguments to the middleware constructor.

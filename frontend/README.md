# frontend

Frontend for the web server.

## Project setup
### Install npm packages
```
npm install
```
If the `package-lock.json` file changes after running just `npm install`, then just revert the changes using git.
If you use `npm install` to add a new package though, do commit the changes to `package-lock.json`


### Compiles and hot-reloads for development
```
npm run serve
```
Use this to run the frontend so you can view it in a browser.

TODO: work something out with configs or .env variables so it can access the backend when run like this (since it'll be using a different port)


### Compiles and minifies for production
```
npm run build
```

The output from this is what the web server needs to serve.

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
